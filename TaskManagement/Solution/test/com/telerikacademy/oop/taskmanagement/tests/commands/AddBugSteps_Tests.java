package com.telerikacademy.oop.taskmanagement.tests.commands;

import com.telerikacademy.oop.taskmanagement.commands.AddBugSteps;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.TaskManagementRepositoryImpl;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class AddBugSteps_Tests {
    private TaskManagementRepository taskManagementRepository;
    private Command testCommand;

    @BeforeEach
    public void before() {

        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.testCommand = new AddBugSteps(taskManagementRepository);
    }

}
