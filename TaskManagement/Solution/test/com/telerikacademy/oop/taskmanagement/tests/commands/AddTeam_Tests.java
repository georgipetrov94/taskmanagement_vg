package com.telerikacademy.oop.taskmanagement.tests.commands;

import com.telerikacademy.oop.taskmanagement.commands.AddTeam;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.TaskManagementRepositoryImpl;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementFactory;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.core.factories.TaskManagementFactoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class AddTeam_Tests {
    private TaskManagementFactory taskManagementFactory;
    private TaskManagementRepository taskManagementRepository;
    private Command testCommand;

    @BeforeEach
    public void before() {

        this.taskManagementFactory = new TaskManagementFactoryImpl();
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.testCommand = new AddTeam(taskManagementFactory, taskManagementRepository);
    }

    @Test
    public void execute_Should_AddNewBug_When_ValidParameters() {

        List<String> arguments = new ArrayList<>();
        arguments.add("name123456");

        testCommand.execute(arguments);
        // Assert
        Assertions.assertEquals("name123456", taskManagementRepository.getTeams().get(taskManagementRepository.getTeams().size() - 1).getName());

    }

    @Test
    public void execute_should_throwException_when_passedMoreArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("name123456");
        arguments.add("description1234567");

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedFewerArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }


}
