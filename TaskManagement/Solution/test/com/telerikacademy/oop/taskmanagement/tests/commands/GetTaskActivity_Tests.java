package com.telerikacademy.oop.taskmanagement.tests.commands;

import com.telerikacademy.oop.taskmanagement.commands.GetTaskActivity;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.TaskManagementRepositoryImpl;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementFactory;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.core.factories.TaskManagementFactoryImpl;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Priority;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Severity;
import com.telerikacademy.oop.taskmanagement.models.contracts.Bug;
import com.telerikacademy.oop.taskmanagement.models.tasks.BugImpl;
import com.telerikacademy.oop.taskmanagement.tests.utils.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class GetTaskActivity_Tests {
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy HH:mm:ss");
    private final LocalDateTime timestamp = LocalDateTime.now();

    private TaskManagementFactory taskManagementFactory;
    private TaskManagementRepository taskManagementRepository;
    private Command testCommand;

    @BeforeEach
    public void before() {

        this.taskManagementFactory = new TaskManagementFactoryImpl();
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.testCommand = new GetTaskActivity(taskManagementRepository);
    }

    @Test
    public void execute_should_throwException_when_passedMoreArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("1");
        arguments.add("1");

        taskManagementRepository.addBug(new BugImpl("name123456", "description123124", Priority.HIGH, Severity.CRITICAL));

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedFewerArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();

        taskManagementRepository.addBug(new BugImpl("name123456", "description123124", Priority.HIGH, Severity.CRITICAL));

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedNonExistingTask() {
        List<String> arguments = new ArrayList<>();
        arguments.add("2");

        taskManagementRepository.addBug(new BugImpl("name123456", "description123124", Priority.HIGH, Severity.CRITICAL));

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

}
