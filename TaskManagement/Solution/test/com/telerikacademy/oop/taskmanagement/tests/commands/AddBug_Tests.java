package com.telerikacademy.oop.taskmanagement.tests.commands;

import com.telerikacademy.oop.taskmanagement.commands.AddBug;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.TaskManagementRepositoryImpl;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Bug;
import com.telerikacademy.oop.taskmanagement.tests.utils.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class AddBug_Tests {
    private TaskManagementRepository taskManagementRepository;
    private Command testCommand;

    @BeforeEach
    public void before() {

        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.testCommand = new AddBug(taskManagementRepository);
    }

    @Test
    public void execute_should_throwException_when_passedMoreArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("name123456");
        arguments.add("description1234567");
        arguments.add("high");
        arguments.add("major");
        arguments.add("high");

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedFewerArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("name123456");
        arguments.add("high");
        arguments.add("major");

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }


    @Test
    public void execute_should_throwException_when_passedWrongPriority() {
        List<String> arguments = new ArrayList<>();
        arguments.add("name123456");
        arguments.add("description1234567");
        arguments.add("hi");
        arguments.add("major");
        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedWrongSeverity() {
        List<String> arguments = new ArrayList<>();
        arguments.add("name123456");
        arguments.add("description1234567");
        arguments.add("high");
        arguments.add("maj");
        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }


    @Test
    public void execute_Should_AddNewBug_When_ValidParameters() {

        List<String> arguments = new ArrayList<>();
        arguments.add("name123456");
        arguments.add("description1234567");
        arguments.add("high");
        arguments.add("major");

        testCommand.execute(arguments);
        Bug bug = Factory.createBug();
        // Assert
        Assertions.assertEquals("name123456", taskManagementRepository.getBugs().get(bug.getTaskId() - 1).getTitle());

    }

}
