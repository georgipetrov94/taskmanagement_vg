package com.telerikacademy.oop.taskmanagement.tests.models;

import com.telerikacademy.oop.taskmanagement.models.common.enums.Priority;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Severity;
import com.telerikacademy.oop.taskmanagement.models.contracts.Bug;
import com.telerikacademy.oop.taskmanagement.models.contracts.Comment;
import com.telerikacademy.oop.taskmanagement.models.contracts.Person;
import com.telerikacademy.oop.taskmanagement.models.contracts.Task;
import com.telerikacademy.oop.taskmanagement.models.tasks.BugImpl;
import com.telerikacademy.oop.taskmanagement.tests.utils.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TaskImpl_Tests {
    @Test
    public void Constructor_Should_ThrowException_WhenTitleLength_Below10() {
        //Arrange,Act,Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> new BugImpl("t", "descriptionHasTenChars", Priority.HIGH, Severity.CRITICAL));
    }

    @Test
    public void Constructor_Should_ThrowException_WhenDescriptionLength_Below10() {
        //Arrange,Act,Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> new BugImpl("titleHasTenChars", "d", Priority.HIGH, Severity.CRITICAL));
    }

    @Test
    public void Constructor_Should_ThrowException_WhenTitleLength_Above50() {
        //Arrange
        StringBuilder sb = new StringBuilder();
        sb.append("a".repeat(51));
        String bigName = sb.toString();
        //Arrange,Act,Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> new BugImpl(bigName, "descriptionHasTenChars", Priority.HIGH, Severity.CRITICAL));
    }

    @Test
    public void Constructor_Should_ThrowException_WhenDescriptionLength_Above500() {
        //Arrange
        StringBuilder sb = new StringBuilder();
        sb.append("a".repeat(501));
        String bigName = sb.toString();
        //Arrange,Act,Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> new BugImpl("titleHasTenChars", bigName, Priority.HIGH, Severity.CRITICAL));
    }



    @Test
    public void AddComment_Should_AddComment() {
        //Arrange
        Comment comment = Factory.createComment();
        Task task = Factory.createBug();

        //Act
        task.addComment(comment, task);

        //Assert
        Assertions.assertEquals(1, task.getComments().size());
        Assertions.assertTrue(task.getComments().contains(comment));
    }

    @Test
    public void RemoveComment_Should_RemoveComment() {
        //Arrange
        Comment comment = Factory.createComment();
        Task task = Factory.createBug();
        task.getComments().add(comment);

        //Act
        task.removeComment(comment, task);

        //Assert
        Assertions.assertEquals(0, task.getComments().size());
        Assertions.assertFalse(task.getComments().contains(comment));
    }

    @Test
    public void getDescription_Should_GetDescription() {
        //Arrange
        Task task = Factory.createBug();

        //Act,Assert
        Assertions.assertEquals("description1234567", task.getDescription());
    }

    @Test
    public void getAssignee_Should_GetAssignee() {
        //Arrange
        Bug task = Factory.createBug();
        Person assignee = Factory.createPerson();

        //Act

        task.setAssignee(assignee);

        //Act,Assert
        Assertions.assertEquals("TestPerson", task.getAssignee().getName());
    }

    @Test
    public void removeAssignee_Should_RemoveAssignee() {
        //Arrange
        Bug task = Factory.createBug();
        Person assignee = Factory.createPerson();
        task.setAssignee(assignee);

        //Act
        task.removeAssignee();

        //Act,Assert
        Assertions.assertEquals("Not assigned", task.getAssignee().getName());
    }


}
