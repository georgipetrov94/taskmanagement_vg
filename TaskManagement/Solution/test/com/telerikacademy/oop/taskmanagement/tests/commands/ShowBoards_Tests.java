package com.telerikacademy.oop.taskmanagement.tests.commands;

import com.telerikacademy.oop.taskmanagement.commands.ShowBoards;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.TaskManagementRepositoryImpl;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementFactory;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.core.factories.TaskManagementFactoryImpl;
import com.telerikacademy.oop.taskmanagement.models.BoardImpl;
import com.telerikacademy.oop.taskmanagement.models.TeamImpl;
import com.telerikacademy.oop.taskmanagement.models.contracts.Board;
import com.telerikacademy.oop.taskmanagement.models.contracts.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowBoards_Tests {
    private TaskManagementRepository taskManagementRepository;
    private Command testCommand;

    @BeforeEach
    public void before() {

        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.testCommand = new ShowBoards(taskManagementRepository);
    }

    @Test
    public void execute_Should_GetBoardActivity_When_ValidParameters() {

        List<String> arguments = new ArrayList<>();
        arguments.add("TeamName");

        Team team = new TeamImpl("TeamName");
        Board board = new BoardImpl("BoardName");

        taskManagementRepository.addTeam(team);
        taskManagementRepository.getTeams().get(0).addBoard(board);

        String output = "Boards in team TeamName: " +
                "\n------------" +
                "\n  BoardName" +
                "\n------------";

        String test = testCommand.execute(arguments);

        // Assert
        Assertions.assertEquals(output, test);
    }

    @Test
    public void execute_Should_GetBoardActivity_When_NoBoardsInTeam() {

        List<String> arguments = new ArrayList<>();
        arguments.add("TeamName");

        Team team = new TeamImpl("TeamName");

        taskManagementRepository.addTeam(team);

        String output = "No boards in this team";

        String test = testCommand.execute(arguments);

        // Assert
        Assertions.assertEquals(output, test);
    }


    @Test
    public void execute_should_throwException_when_passedMoreArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("TeamName");
        arguments.add("TeamName");

        Team team = new TeamImpl("TeamName");
        Board board = new BoardImpl("BoardName");

        taskManagementRepository.addTeam(team);
        taskManagementRepository.getTeams().get(0).addBoard(board);

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedFewerArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();

        Team team = new TeamImpl("TeamName");
        Board board = new BoardImpl("BoardName");

        taskManagementRepository.addTeam(team);
        taskManagementRepository.getTeams().get(0).addBoard(board);

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }


}
