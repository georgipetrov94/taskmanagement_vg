package com.telerikacademy.oop.taskmanagement.tests.commands;

import com.telerikacademy.oop.taskmanagement.commands.AddBoard;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.TaskManagementRepositoryImpl;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.TeamImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class AddBoard_Tests {
    private TaskManagementRepository taskManagementRepository;
    private Command testCommand;

    @BeforeEach
    public void before() {

        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.testCommand = new AddBoard(taskManagementRepository);
    }

    @Test
    public void execute_should_throwException_when_passedMoreArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("TestTeam");
        arguments.add("BoardBoard");
        arguments.add("oneMoreArg");
        taskManagementRepository.addTeam(new TeamImpl("TestTeamButNo"));

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedFewerArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("TestTeam");
        taskManagementRepository.addTeam(new TeamImpl("TestTeamButNo"));

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedWrongTeam() {
        List<String> arguments = new ArrayList<>();
        arguments.add("TestTeam");
        arguments.add("BoardBoard");
        taskManagementRepository.addTeam(new TeamImpl("TestTeamButNo"));
        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }


    @Test
    public void execute_Should_AddNewBoard_When_ValidParameters() {

        List<String> arguments = new ArrayList<>();
        arguments.add("TestTeam");
        arguments.add("BoardBoard");
        taskManagementRepository.addTeam(new TeamImpl("TestTeam"));

        testCommand.execute(arguments);
        // Assert
        Assertions.assertEquals("BoardBoard", taskManagementRepository.getTeams().get(0).getBoards().get(0).getName());

    }

}
