package com.telerikacademy.oop.taskmanagement.tests.commands;

import com.telerikacademy.oop.taskmanagement.commands.AddComment;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.TaskManagementRepositoryImpl;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementFactory;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.core.factories.TaskManagementFactoryImpl;
import com.telerikacademy.oop.taskmanagement.models.PersonImpl;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Priority;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Severity;
import com.telerikacademy.oop.taskmanagement.models.contracts.Bug;
import com.telerikacademy.oop.taskmanagement.models.tasks.BugImpl;
import com.telerikacademy.oop.taskmanagement.tests.utils.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class AddComment_Tests {
    private TaskManagementFactory taskManagementFactory;
    private TaskManagementRepository taskManagementRepository;
    private Command testCommand;

    @BeforeEach
    public void before() {

        this.taskManagementFactory = new TaskManagementFactoryImpl();
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.testCommand = new AddComment(taskManagementFactory, taskManagementRepository);
    }

    @Test
    public void execute_Should_AddComment_When_ValidParameters() {

        Bug bug = Factory.createBug();

        List<String> arguments = new ArrayList<>();
        arguments.add("Comment!2345235235");
        arguments.add(String.valueOf(bug.getTaskId()+1));
        arguments.add("gosho");
        taskManagementRepository.addPerson(new PersonImpl("gosho"));
        taskManagementRepository.addBug(new BugImpl("name123456", "description12345", Priority.HIGH, Severity.CRITICAL));

        testCommand.execute(arguments);
        // Assert
        Assertions.assertEquals("Comment!2345235235", taskManagementRepository.getBugs().get(bug.getTaskId()+1).getComments().get(0).getContent());

    }

    @Test
    public void execute_should_throwException_when_passedWrongTaskID() {

        List<String> arguments = new ArrayList<>();
        arguments.add("Comment!2345235235");
        arguments.add("1");
        arguments.add("gosho");
        taskManagementRepository.addPerson(new PersonImpl("gosho"));

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedWrongPerson() {

        List<String> arguments = new ArrayList<>();
        arguments.add("Comment!2345235235");
        arguments.add("1");
        arguments.add("pesho");
        taskManagementRepository.addBug(new BugImpl("name123456", "description12345", Priority.HIGH, Severity.CRITICAL));
        taskManagementRepository.addPerson(new PersonImpl("gosho"));

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedWrongIDtype() {

        List<String> arguments = new ArrayList<>();
        arguments.add("Comment!2345235235");
        arguments.add("a");
        arguments.add("gosho");
        taskManagementRepository.addBug(new BugImpl("name123456", "description12345", Priority.HIGH, Severity.CRITICAL));
        taskManagementRepository.addPerson(new PersonImpl("gosho"));

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

}
