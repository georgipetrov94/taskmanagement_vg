package com.telerikacademy.oop.taskmanagement.tests.models;

import com.telerikacademy.oop.taskmanagement.models.common.enums.BugStatus;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Priority;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Severity;
import com.telerikacademy.oop.taskmanagement.models.contracts.Bug;
import com.telerikacademy.oop.taskmanagement.tests.utils.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class BugImpl_Tests {


    @Test
    public void bug_Should_AdvanceStatus() {

        Bug bug = Factory.createBug();
        bug.advanceStatus();
        Assertions.assertEquals(BugStatus.FIXED, bug.getStatus());
    }

    @Test
    public void bug_Should_RevertStatus() {

        Bug bug = Factory.createBug();

        bug.advanceStatus();
        bug.revertStatus();
        Assertions.assertEquals(BugStatus.ACTIVE, bug.getStatus());
    }

    @Test
    public void bug_Should_RevertPriority() {

        Bug bug = Factory.createBug();

        bug.revertPriority();
        Assertions.assertEquals(Priority.MEDIUM, bug.getPriority());
    }

    @Test
    public void bug_Should_AdvancePriority() {

        Bug bug = Factory.createBug();

        bug.revertPriority();
        bug.advancePriority();
        Assertions.assertEquals(Priority.HIGH, bug.getPriority());
    }

    @Test
    public void bug_Should_RevertSeverity() {

        Bug bug = Factory.createBug();

        bug.revertSeverity();
        Assertions.assertEquals(Severity.MINOR, bug.getSeverity());
    }

    @Test
    public void bug_Should_AdvanceSeverity() {

        Bug bug = Factory.createBug();
        bug.advanceSeverity();
        Assertions.assertEquals(Severity.CRITICAL, bug.getSeverity());
    }


}
