package com.telerikacademy.oop.taskmanagement.tests.commands;

import com.telerikacademy.oop.taskmanagement.commands.UnassignTask;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.TaskManagementRepositoryImpl;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.PersonImpl;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Priority;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Severity;
import com.telerikacademy.oop.taskmanagement.models.contracts.Bug;
import com.telerikacademy.oop.taskmanagement.models.tasks.BugImpl;
import com.telerikacademy.oop.taskmanagement.tests.utils.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class UnassignTask_Tests {
    private TaskManagementRepository taskManagementRepository;
    private Command testCommand;

    @BeforeEach
    public void before() {

        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.testCommand = new UnassignTask(taskManagementRepository);
    }

    @Test
    public void execute_Should_UnassignPersonToTask_When_ValidParameters() {

        Bug bug = Factory.createBug();

        List<String> arguments = new ArrayList<>();
        arguments.add("gosho");
        arguments.add(String.valueOf(bug.getTaskId() + 1));

        taskManagementRepository.addBug(new BugImpl("name123456", "description12345", Priority.HIGH, Severity.CRITICAL));
        taskManagementRepository.addPerson(new PersonImpl("gosho"));
        taskManagementRepository.getPeople().get(0).addTask(bug);

        testCommand.execute(arguments);
        // Assert
        Assertions.assertEquals("Not assigned", taskManagementRepository.getBugs().get(bug.getTaskId() + 1).getAssignee().getName());

    }

    @Test
    public void execute_should_throwException_when_passedMoreArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("gosho");
        arguments.add("1");
        arguments.add("1");

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedFewerArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("gosho");

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }


}
