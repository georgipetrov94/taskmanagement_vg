package com.telerikacademy.oop.taskmanagement.tests.commands;

import com.telerikacademy.oop.taskmanagement.commands.AddPersonTeam;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.TaskManagementRepositoryImpl;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.PersonImpl;
import com.telerikacademy.oop.taskmanagement.models.TeamImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class AddPersonTeam_Tests {
    private TaskManagementRepository taskManagementRepository;
    private Command testCommand;

    @BeforeEach
    public void before() {

        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.testCommand = new AddPersonTeam(taskManagementRepository);
    }

    @Test
    public void execute_Should_AddPersonToTeam_When_ValidParameters() {

        List<String> arguments = new ArrayList<>();
        arguments.add("TeamName123456");
        arguments.add("PersonName");

        taskManagementRepository.addPerson(new PersonImpl("PersonName"));
        taskManagementRepository.addTeam(new TeamImpl("TeamName123456"));

        StringBuilder str = new StringBuilder();
        str.append("People in team TeamName123456: ");
        str.append("\n------------");
        str.append("\n  ").append("PersonName");
        str.append("\n------------");


        testCommand.execute(arguments);

        Assertions.assertEquals(str.toString(), taskManagementRepository.getTeams().get(0).showTeamMembers());
    }

    @Test
    public void execute_should_throwException_when_passedMoreArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("TeamName123456");
        arguments.add("PersonName");
        arguments.add("PersonName");


        taskManagementRepository.addPerson(new PersonImpl("PersonName"));
        taskManagementRepository.addTeam(new TeamImpl("TeamName123456"));


        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedFewerArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("TeamName123456");

        taskManagementRepository.addPerson(new PersonImpl("PersonName"));
        taskManagementRepository.addTeam(new TeamImpl("TeamName123456"));


        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedNonExistingPerson() {
        List<String> arguments = new ArrayList<>();
        arguments.add("TeamName123456");
        arguments.add("PersonName");

        taskManagementRepository.addTeam(new TeamImpl("TeamName123456"));


        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedNonExistingTeam() {
        List<String> arguments = new ArrayList<>();
        arguments.add("TeamName123456");
        arguments.add("PersonName");

        taskManagementRepository.addPerson(new PersonImpl("PersonName"));


        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }


}
