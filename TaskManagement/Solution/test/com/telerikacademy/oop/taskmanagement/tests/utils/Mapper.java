package com.telerikacademy.oop.taskmanagement.tests.utils;

import com.telerikacademy.oop.taskmanagement.models.contracts.*;
import com.telerikacademy.oop.taskmanagement.models.tasks.BugImpl;
import com.telerikacademy.oop.taskmanagement.models.tasks.TasksImpl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static java.lang.String.valueOf;
import static java.util.Arrays.asList;

public class Mapper {

    public static List<String> mapTaskToParamsList(Task v) {
        return new LinkedList<>(asList(v.getTitle(), v.getDescription()));
    }

    public static List<String> mapBugToParamsList(Bug b) {
        List<String> result = mapTaskToParamsList(b);
        result.add(b.getPriority().toString());
        result.add(b.getSeverity().toString());
        return result;
    }

    public static List<String> mapStoryToParamsList(Story s) {
        List<String> result = mapTaskToParamsList(s);
        result.add(s.getPriority().toString());
        result.add(s.getSize().toString());
        result.add(s.getStatus().toString());
        return result;
    }

    public static List<String> mapCommentToParamsList(Comment c) {
        List<String> result = new ArrayList<>();
        result.add(c.getAuthor());
        result.add(c.getContent());
        return result;
    }


    public static List<String> mapFeedbackToParamsList(Feedback f) {
        List<String> result = mapTaskToParamsList(f);
        result.add(Integer.toString(f.getRating()));
        result.add(f.getStatus().toString());
        return result;
    }

    public static List<String> mapPersonToParamsList(Person p) {
        return new LinkedList<>(asList(p.getName()));
    }

    public static List<String> mapTeamToParamsList(Team t) {
        return new LinkedList<>(asList(t.getName()));
    }

    public static List<String> mapBoardToParamsList(Team t, Board b) {
        List<String> result = new ArrayList<>(asList(t.getName(), b.getName()));
        return result;
    }


}
