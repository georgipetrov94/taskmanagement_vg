package com.telerikacademy.oop.taskmanagement.tests.utils;

import com.telerikacademy.oop.taskmanagement.models.BaseMemberBoardImpl;
import com.telerikacademy.oop.taskmanagement.models.BoardImpl;
import com.telerikacademy.oop.taskmanagement.models.PersonImpl;
import com.telerikacademy.oop.taskmanagement.models.TeamImpl;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Priority;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Severity;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Size;
import com.telerikacademy.oop.taskmanagement.models.contracts.*;
import com.telerikacademy.oop.taskmanagement.models.tasks.BugImpl;
import com.telerikacademy.oop.taskmanagement.models.tasks.CommentImpl;
import com.telerikacademy.oop.taskmanagement.models.tasks.FeedbackImpl;
import com.telerikacademy.oop.taskmanagement.models.tasks.StoryImpl;

public class Factory {

    public static Person createPerson() {
        return new PersonImpl("TestPerson");
    }

    public static Team createTeam() {
        return new TeamImpl("TestTeam");
    }

    public static Comment createComment() {
        return new CommentImpl("THIS IS TEST COMMENT", "Gosho");
    }

    public static Story createStory() {
        return new StoryImpl("BigStoryWow", "DescriptionDescription", Priority.LOW, Size.MEDIUM);
    }

    public static Feedback createFeedback() {
        return new FeedbackImpl("Feedbackname1", "wowDescriptionMuchGood", 8);
    }

    public static Bug createBug() {
        return new BugImpl("name123456", "description1234567", Priority.HIGH, Severity.MAJOR);
    }


}
