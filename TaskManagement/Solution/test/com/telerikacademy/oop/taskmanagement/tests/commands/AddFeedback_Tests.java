package com.telerikacademy.oop.taskmanagement.tests.commands;

import com.telerikacademy.oop.taskmanagement.commands.AddFeedback;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.TaskManagementRepositoryImpl;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Feedback;
import com.telerikacademy.oop.taskmanagement.tests.utils.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class AddFeedback_Tests {
    private TaskManagementRepository taskManagementRepository;
    private Command testCommand;

    @BeforeEach
    public void before() {

        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.testCommand = new AddFeedback(taskManagementRepository);
    }

    @Test
    public void execute_Should_AddNewFeedback_When_ValidParameters() {

        List<String> arguments = new ArrayList<>();
        arguments.add("name1234567");
        arguments.add("description1234567");
        arguments.add("10");

        testCommand.execute(arguments);
        Feedback feedback = Factory.createFeedback();
        // Assert
        Assertions.assertEquals("name1234567", taskManagementRepository.getFeedbacks().get(feedback.getTaskId() - 1).getTitle());

    }

    @Test
    public void execute_should_throwException_when_passedWrongFeedbackType() {
        List<String> arguments = new ArrayList<>();
        arguments.add("name1234567");
        arguments.add("description1234567");
        arguments.add("gosho");
        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedMoreArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("name1234567");
        arguments.add("description1234567");
        arguments.add("10");
        arguments.add("10");


        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedFewerArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("name1234567");
        arguments.add("description1234567");

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }


}
