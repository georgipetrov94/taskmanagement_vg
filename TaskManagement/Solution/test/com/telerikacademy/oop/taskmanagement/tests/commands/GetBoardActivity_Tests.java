package com.telerikacademy.oop.taskmanagement.tests.commands;

import com.telerikacademy.oop.taskmanagement.commands.GetBoardActivity;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.TaskManagementRepositoryImpl;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.BoardImpl;
import com.telerikacademy.oop.taskmanagement.models.TeamImpl;
import com.telerikacademy.oop.taskmanagement.models.contracts.Board;
import com.telerikacademy.oop.taskmanagement.models.contracts.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class GetBoardActivity_Tests {
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy HH:mm:ss");
    private final LocalDateTime timestamp = LocalDateTime.now();

    private TaskManagementRepository taskManagementRepository;
    private Command testCommand;

    @BeforeEach
    public void before() {

        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.testCommand = new GetBoardActivity(taskManagementRepository);
    }

    @Test
    public void execute_Should_GetBoardActivity_When_ValidParameters() {

        List<String> arguments = new ArrayList<>();
        arguments.add("TeamName");
        arguments.add("BoardName");

        Team team = new TeamImpl("TeamName");
        Board board = new BoardImpl("BoardName");

        taskManagementRepository.addTeam(team);
        taskManagementRepository.getTeams().get(0).addBoard(board);
        String output = String.format("[%s] Board {{BoardName}} was created%n", timestamp.format(formatter));

        String test = testCommand.execute(arguments);

        // Assert
        Assertions.assertEquals(output, test);
    }

    @Test
    public void execute_should_throwException_when_passedMoreArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("TeamName");
        arguments.add("BoardName");
        arguments.add("BoardName");

        Team team = new TeamImpl("TeamName");
        Board board = new BoardImpl("BoardName");

        taskManagementRepository.addTeam(team);
        taskManagementRepository.getTeams().get(0).addBoard(board);

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedFewerArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("TeamName");

        Team team = new TeamImpl("TeamName");
        Board board = new BoardImpl("BoardName");

        taskManagementRepository.addTeam(team);
        taskManagementRepository.getTeams().get(0).addBoard(board);

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }


}
