package com.telerikacademy.oop.taskmanagement.tests.models;

import com.telerikacademy.oop.taskmanagement.models.common.enums.Priority;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Size;
import com.telerikacademy.oop.taskmanagement.models.common.enums.StoryStatus;
import com.telerikacademy.oop.taskmanagement.models.contracts.Story;
import com.telerikacademy.oop.taskmanagement.tests.utils.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class StoryImpl_Tests {

    @Test
    public void story_Should_AdvanceStatus() {

        Story story = Factory.createStory();
        story.advanceStatus();
        Assertions.assertEquals(StoryStatus.INPROGRESS, story.getStatus());
    }

    @Test
    public void story_Should_RevertStatus() {

        Story story = Factory.createStory();

        story.advanceStatus();
        story.revertStatus();
        Assertions.assertEquals(StoryStatus.NOTDONE, story.getStatus());
    }

    @Test
    public void story_Should_RevertPriority() {

        Story story = Factory.createStory();

        story.revertPriority();
        Assertions.assertEquals(Priority.LOW, story.getPriority());
    }

    @Test
    public void story_Should_AdvancePriority() {

        Story story = Factory.createStory();

        story.revertPriority();
        story.advancePriority();
        Assertions.assertEquals(Priority.MEDIUM, story.getPriority());
    }

    @Test
    public void story_Should_RevertSize() {

        Story story = Factory.createStory();

        story.revertSize();
        Assertions.assertEquals(Size.SMALL, story.getSize());
    }

    @Test
    public void story_Should_AdvanceSize() {

        Story story = Factory.createStory();

        story.advanceSize();
        Assertions.assertEquals(Size.LARGE, story.getSize());
    }
}
