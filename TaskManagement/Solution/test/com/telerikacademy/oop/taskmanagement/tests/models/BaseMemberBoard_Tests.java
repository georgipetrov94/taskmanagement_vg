package com.telerikacademy.oop.taskmanagement.tests.models;

import com.telerikacademy.oop.taskmanagement.models.PersonImpl;
import com.telerikacademy.oop.taskmanagement.models.contracts.Person;
import com.telerikacademy.oop.taskmanagement.models.contracts.Task;
import com.telerikacademy.oop.taskmanagement.tests.utils.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BaseMemberBoard_Tests {


    @Test
    public void Constructor_Should_ThrowException_WhenNameLength_Below5() {
        //Arrange,Act,Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> new PersonImpl("a"));
    }

    @Test
    public void Constructor_Should_ThrowException_WhenNameLength_Above15() {
        //Arrange,Act,Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> new PersonImpl("gogogogogogogogogogogogogogogogoog"));
    }

    @Test
    public void AddTask_Should_AddTask() {
        //Arrange
        Person person = Factory.createPerson();
        Task bug = Factory.createBug();

        //Act
        person.addTask(bug);

        //Assert
        Assertions.assertEquals(1, person.getTasks().size());
        Assertions.assertTrue(person.getTasks().containsKey(bug.getTaskId()));
    }

    @Test
    public void RemoveTask_Should_RemoveTask() {
        //Arrange
        Person person = Factory.createPerson();
        Task bug = Factory.createBug();
        person.addTask(bug);

        //Act
        person.removeTask(bug);

        //Assert
        Assertions.assertEquals(0, person.getTasks().size());
        Assertions.assertFalse(person.getTasks().containsKey(bug.getTaskId()));

    }


}
