package com.telerikacademy.oop.taskmanagement.tests.commands;

import com.telerikacademy.oop.taskmanagement.commands.AssignTaskToBoard;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.TaskManagementRepositoryImpl;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementFactory;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.core.factories.TaskManagementFactoryImpl;
import com.telerikacademy.oop.taskmanagement.models.BoardImpl;
import com.telerikacademy.oop.taskmanagement.models.PersonImpl;
import com.telerikacademy.oop.taskmanagement.models.TeamImpl;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Priority;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Severity;
import com.telerikacademy.oop.taskmanagement.models.contracts.Bug;
import com.telerikacademy.oop.taskmanagement.models.contracts.Team;
import com.telerikacademy.oop.taskmanagement.models.tasks.BugImpl;
import com.telerikacademy.oop.taskmanagement.tests.utils.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class AssignTaskToBoard_Tests {
    private TaskManagementFactory taskManagementFactory;
    private TaskManagementRepository taskManagementRepository;
    private Command testCommand;

    @BeforeEach
    public void before() {

        this.taskManagementFactory = new TaskManagementFactoryImpl();
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.testCommand = new AssignTaskToBoard(taskManagementRepository);
    }


    @Test
    public void execute_Should_AssignTaskToBoard_When_ValidParameters() {

        Bug bug = Factory.createBug();
        Team team = new TeamImpl("TestTeam");
        taskManagementRepository.addTeam(team);

        List<String> arguments = new ArrayList<>();
        arguments.add("TestTeam");
        arguments.add("BoardName");
        arguments.add(String.valueOf(bug.getTaskId() + 1));

        taskManagementRepository.addBug(new BugImpl("name123456", "description12345", Priority.HIGH, Severity.CRITICAL));
        team.addBoard(new BoardImpl("BoardName"));

        testCommand.execute(arguments);
        // Assert
        Assertions.assertEquals("name123456", team.getBoards().get(0).getTasks().get(bug.getTaskId()+1).getTitle());

    }

    @Test
    public void execute_should_throwException_when_passedMoreArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("TestTeam");
        arguments.add("BoardName");
        arguments.add("1");
        arguments.add("1");

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedFewerArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("TestTeam");
        arguments.add("BoardName");

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }


}
