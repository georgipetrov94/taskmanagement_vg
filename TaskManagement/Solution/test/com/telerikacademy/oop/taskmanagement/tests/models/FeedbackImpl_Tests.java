package com.telerikacademy.oop.taskmanagement.tests.models;

import com.telerikacademy.oop.taskmanagement.models.common.enums.FeedbackStatus;
import com.telerikacademy.oop.taskmanagement.models.contracts.Feedback;
import com.telerikacademy.oop.taskmanagement.tests.utils.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class FeedbackImpl_Tests {

    @Test
    public void feedback_Should_AdvanceStatus() {

        Feedback feedbackback = Factory.createFeedback();
        feedbackback.advanceStatus();
        Assertions.assertEquals(FeedbackStatus.UNSCHEDULED, feedbackback.getStatus());
    }

    @Test
    public void feedback_Should_RevertStatus() {

        Feedback feedbackback = Factory.createFeedback();
        feedbackback.advanceStatus();
        feedbackback.revertStatus();
        Assertions.assertEquals(FeedbackStatus.NEW, feedbackback.getStatus());
    }

    @Test
    public void feedback_Should_ChangeRating() {

        Feedback feedbackback = Factory.createFeedback();
        feedbackback.changeRating(4);
        Assertions.assertEquals(4, feedbackback.getRating());
    }

}
