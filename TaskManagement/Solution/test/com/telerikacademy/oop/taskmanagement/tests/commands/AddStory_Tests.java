package com.telerikacademy.oop.taskmanagement.tests.commands;

import com.telerikacademy.oop.taskmanagement.commands.AddStory;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.TaskManagementRepositoryImpl;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Story;
import com.telerikacademy.oop.taskmanagement.tests.utils.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class AddStory_Tests {
    private TaskManagementRepository taskManagementRepository;
    private Command testCommand;

    @BeforeEach
    public void before() {

        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.testCommand = new AddStory(taskManagementRepository);
    }

    @Test
    public void execute_should_throwException_when_passedMoreArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("name123456");
        arguments.add("description1234567");
        arguments.add("high");
        arguments.add("small");
        arguments.add("small");

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedFewerArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("name123456");
        arguments.add("high");
        arguments.add("small");

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }


    @Test
    public void execute_should_throwException_when_passedWrongPriority() {
        List<String> arguments = new ArrayList<>();
        arguments.add("name123456");
        arguments.add("description1234567");
        arguments.add("hi");
        arguments.add("small");
        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedWrongSize() {
        List<String> arguments = new ArrayList<>();
        arguments.add("name123456");
        arguments.add("description1234567");
        arguments.add("high");
        arguments.add("gosho");
        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }


    @Test
    public void execute_Should_AddNewStory_When_ValidParameters() {

        List<String> arguments = new ArrayList<>();
        arguments.add("name123456");
        arguments.add("description1234567");
        arguments.add("high");
        arguments.add("small");

        testCommand.execute(arguments);
        Story story = Factory.createStory();
        // Assert
        Assertions.assertEquals("name123456", taskManagementRepository.getStories().get(story.getTaskId() - 1).getTitle());

    }


}
