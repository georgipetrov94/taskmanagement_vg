package com.telerikacademy.oop.taskmanagement.tests.commands;

import com.telerikacademy.oop.taskmanagement.commands.GetTeamActivity;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.TaskManagementRepositoryImpl;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementFactory;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.core.factories.TaskManagementFactoryImpl;
import com.telerikacademy.oop.taskmanagement.models.TeamImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class GetTeamActivity_Tests {
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy HH:mm:ss");
    private final LocalDateTime timestamp = LocalDateTime.now();

    private TaskManagementRepository taskManagementRepository;
    private Command testCommand;

    @BeforeEach
    public void before() {

        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.testCommand = new GetTeamActivity(taskManagementRepository);
    }

    @Test
    public void execute_Should_GetTeamActivity_When_ValidParameters() {

        List<String> arguments = new ArrayList<>();
        arguments.add("TeamName");

        taskManagementRepository.addTeam(new TeamImpl("TeamName"));

        String output = String.format("[%s] Team {{TeamName}} was created%n", timestamp.format(formatter));

        String test = testCommand.execute(arguments);

        // Assert
        Assertions.assertEquals(output, test);
    }

    @Test
    public void execute_should_throwException_when_passedMoreArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();
        arguments.add("TeamName");
        arguments.add("TeamName");

        taskManagementRepository.addTeam(new TeamImpl("TeamName"));

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedFewerArgumentsThanExpected() {
        List<String> arguments = new ArrayList<>();

        taskManagementRepository.addTeam(new TeamImpl("TeamName"));

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_passedNonExistingTeam() {
        List<String> arguments = new ArrayList<>();
        arguments.add("TeamName");

        taskManagementRepository.addTeam(new TeamImpl("Gosho"));

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }


}
