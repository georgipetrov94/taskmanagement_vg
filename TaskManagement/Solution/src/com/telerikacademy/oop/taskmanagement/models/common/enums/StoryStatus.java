package com.telerikacademy.oop.taskmanagement.models.common.enums;

public enum StoryStatus {
    NOTDONE, INPROGRESS, DONE;

    @Override
    public String toString() {
        switch (this) {
            case NOTDONE:
                return "Not done";
            case INPROGRESS:
                return "In progress";
            case DONE:
                return "Done";
            default:
                return "";
        }
    }
}
