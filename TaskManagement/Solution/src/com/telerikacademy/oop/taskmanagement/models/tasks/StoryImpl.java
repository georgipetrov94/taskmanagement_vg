package com.telerikacademy.oop.taskmanagement.models.tasks;

import com.telerikacademy.oop.taskmanagement.models.PersonImpl;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Priority;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Size;
import com.telerikacademy.oop.taskmanagement.models.common.enums.StoryStatus;
import com.telerikacademy.oop.taskmanagement.models.contracts.Person;
import com.telerikacademy.oop.taskmanagement.models.contracts.Story;

public class StoryImpl extends TasksImpl implements Story {

    private final static String TASK_TYPE = "Story";

    private Priority priority;
    private Size size;
    private StoryStatus status;
    private Person assignee;


    public StoryImpl(String title, String description, Priority priority, Size size) {
        super(title, description);
        setPriority(priority);

        setStatus(StoryStatus.NOTDONE);
        setSize(size);
        setAssignee(new PersonImpl("Not assigned"));
        logEvent(String.format("Task created: %s", viewInfo()));

    }

    @Override
    public void setAssignee(Person assignee) {
        this.assignee = assignee;
    }

    @Override
    public Person getAssignee() {
        return assignee;
    }

    @Override
    public void removeAssignee() {
        this.assignee = new PersonImpl("Not assigned");
    }

    private void setPriority(Priority priority) {
        this.priority = priority;
    }

    private void setSize(Size size) {
        this.size = size;
    }

    private void setStatus(StoryStatus status) {
        this.status = status;
    }

    public StoryStatus getStatus() {
        return status;
    }

    public Priority getPriority() {
        return priority;
    }

    public Size getSize() {
        return size;
    }

    @Override
    public String print() {
        return String.format(basePrint() +
                        "Priority: %s\n" +
                        "Size: %s\n" +
                        "Assigned to : %s\n" +
                        "Comments : %s", getTaskId(), getTitle(), TASK_TYPE, getDescription(), TASK_TYPE, getStatus().toString(), getPriority().toString(), getSize(),
                getAssignee(), showComments());

    }

    @Override
    public String viewInfo() {
        return String.format("Story {{%s}} was created", getTitle());
    }

    @Override
    public void advanceStatus() {
        if (status != StoryStatus.DONE) {
            logEvent(String.format("Story status changed from %s to %s", getStatus(), StoryStatus.values()[status.ordinal() + 1]));
            setStatus(StoryStatus.values()[status.ordinal() + 1]);
            System.out.println("Status changed");
        } else {
            System.out.printf("Can't advance, already at %s%n", getStatus());
        }
    }

    @Override
    public void revertStatus() {
        if (status != StoryStatus.NOTDONE) {
            logEvent(String.format("Story status changed from %s to %s", getStatus(), StoryStatus.values()[status.ordinal() - 1]));
            setStatus(StoryStatus.values()[status.ordinal() - 1]);
            System.out.println("Status changed");
        } else {
            System.out.printf("Can't revert, already at %s%n", getStatus());
        }
    }

    @Override
    public void advancePriority() {
        if (priority != Priority.HIGH) {
            logEvent(String.format("Story priority changed from %s to %s", getPriority(), Priority.values()[priority.ordinal() + 1]));
            setPriority(Priority.values()[priority.ordinal() + 1]);
            System.out.println("Priority changed");
        } else {
            System.out.printf("Can't advance, already at %s%n", getPriority());
        }
    }

    @Override
    public void revertPriority() {
        if (priority != Priority.LOW) {
            logEvent(String.format("Story priority changed from %s to %s", getPriority(), Priority.values()[priority.ordinal() - 1]));
            setPriority(Priority.values()[priority.ordinal() - 1]);
            System.out.println("Priority changed");
        } else {
            System.out.printf("Can't revert, already at %s%n", getPriority());
        }
    }


    @Override
    public void advanceSize() {
        if (size != Size.LARGE) {
            logEvent(String.format("Story size changed from %s to %s", getSize(), Size.values()[size.ordinal() + 1]));
            setSize(Size.values()[size.ordinal() + 1]);
            System.out.println("Size changed");
        } else {
            System.out.printf("Can't advance, already at %s%n", getSize());
        }
    }

    @Override
    public void revertSize() {
        if (size != Size.SMALL) {
            logEvent(String.format("Story size changed from %s to %s", getSize(), Size.values()[size.ordinal() - 1]));
            setSize(Size.values()[size.ordinal() - 1]);
            System.out.println("Size changed");
        } else {
            System.out.printf("Can't revert, already at %s%n", getSize());
        }
    }

}
