package com.telerikacademy.oop.taskmanagement.models.contracts;

public interface Comment {
    String getContent();

    String getAuthor();
}
