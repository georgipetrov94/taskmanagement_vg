package com.telerikacademy.oop.taskmanagement.models.common;


public class Validator {
    
    public static void validateIntRange(int value, int min, int max, String message) {
        if (value < min || value > max) {
            throw new IllegalArgumentException(message);
        }
    }
    
}
