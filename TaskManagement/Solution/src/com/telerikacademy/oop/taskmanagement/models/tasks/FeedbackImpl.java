package com.telerikacademy.oop.taskmanagement.models.tasks;

import com.telerikacademy.oop.taskmanagement.models.common.enums.FeedbackStatus;
import com.telerikacademy.oop.taskmanagement.models.contracts.Feedback;

public class FeedbackImpl extends TasksImpl implements Feedback {

    private final static String TASK_TYPE = "Feedback";

    private int rating;
    private FeedbackStatus status;

    public FeedbackImpl(String title, String description, int rating) {
        super(title, description);
        setRating(rating);
        setStatus(FeedbackStatus.NEW);

        logEvent(String.format("Task created: %s", viewInfo()));

    }

    private void setRating(int rating) {
        this.rating = rating;
    }

    private void setStatus(FeedbackStatus status) {
        this.status = status;
    }

    @Override
    public int getRating() {
        return rating;
    }

    @Override
    public FeedbackStatus getStatus() {
        return status;
    }

    @Override
    public String print() {
        return String.format(basePrint() +
                        "Rating: %d\n" +
                        "Comments : %s", getTaskId(), getTitle(), TASK_TYPE, getDescription(),
                TASK_TYPE, getStatus().toString(), getRating(), showComments());

    }

    @Override
    public String viewInfo() {
        return String.format("Feedback {{%s}} was created", getTitle());
    }

    @Override
    public void advanceStatus() {
        if (status != FeedbackStatus.DONE) {
            logEvent(String.format("Feedback status changed from %s to %s", getStatus(), FeedbackStatus.values()[status.ordinal() + 1]));
            setStatus(FeedbackStatus.values()[status.ordinal() + 1]);
            System.out.println("Status changed");
        } else {
            System.out.printf("Can't advance, already at %s%n", getStatus());
        }
    }

    @Override
    public void revertStatus() {
        if (status != FeedbackStatus.NEW) {
            logEvent(String.format("Feedback status changed from %s to %s", getStatus(), FeedbackStatus.values()[status.ordinal() - 1]));
            setStatus(FeedbackStatus.values()[status.ordinal() - 1]);
            System.out.println("Status changed");
        } else {
            System.out.printf("Can't revert, already at %s%n", getStatus());
        }
    }


    @Override
    public void changeRating(int i) {
        if (i >= 0 && i <= 10) {
            logEvent(String.format("Feedback rating changed from %s to %s", getRating(), i));
            setRating(i);
            System.out.println("Rating changed to: " + i);
        } else {
            throw new IllegalArgumentException("Rating can be from 0 to 10");
        }
    }
}
