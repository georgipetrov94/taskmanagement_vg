package com.telerikacademy.oop.taskmanagement.models.contracts;

import com.telerikacademy.oop.taskmanagement.models.common.enums.BugStatus;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Priority;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Severity;

import java.util.List;


public interface Bug extends Assignable {

    Priority getPriority();

    Severity getSeverity();

    BugStatus getStatus();

    void advancePriority();

    void revertPriority();

    void advanceSeverity();

    void revertSeverity();

    List<String> getSteps();

    void addStep(String step);

    void setAssignee(Person assignee);

    Person getAssignee();

    void removeAssignee();
}
