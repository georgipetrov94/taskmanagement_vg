package com.telerikacademy.oop.taskmanagement.models.tasks;

import com.telerikacademy.oop.taskmanagement.models.PersonImpl;
import com.telerikacademy.oop.taskmanagement.models.common.Validator;
import com.telerikacademy.oop.taskmanagement.models.contracts.Comment;
import com.telerikacademy.oop.taskmanagement.models.contracts.Person;
import com.telerikacademy.oop.taskmanagement.models.contracts.Task;
import com.telerikacademy.oop.taskmanagement.models.logger.EventLog;

import java.util.ArrayList;
import java.util.List;


public abstract class TasksImpl implements Task {

    private static final int TITLE_MIN_LENGTH = 10;
    private static final int TITLE_MAX_LENGTH = 50;
    private static final String TITLE_INVALID_LENGTH = String.format("Title should have between %d and %d characters!", TITLE_MIN_LENGTH, TITLE_MAX_LENGTH);


    private static final int DESCRIPTION_MIN_LENGTH = 10;
    private static final int DESCRIPTION_MAX_LENGTH = 500;
    private static final String DESCRIPTION_INVALID_LENGTH = String.format("Description should have between %d and %d characters!", DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH);


    private static int id = 1;
    private final int taskId = id;


    private final List<Comment> comments;
    private String title;
    private String description;
    private final List<EventLog> history = new ArrayList<>();

    public TasksImpl(String title, String description) {
        setTitle(title);
        setDescription(description);
        id++;
        comments = new ArrayList<>();

    }


    private void setTitle(String title) {
        Validator.validateIntRange(title.length(), TITLE_MIN_LENGTH, TITLE_MAX_LENGTH, TITLE_INVALID_LENGTH);
        this.title = title;
    }

    private void setDescription(String description) {
        Validator.validateIntRange(description.length(), DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH, DESCRIPTION_INVALID_LENGTH);
        this.description = description;
    }

    @Override
    public int getTaskId() {
        return taskId;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }


    @Override
    public List<Comment> getComments() {
        return comments;
    }

    @Override
    public String showComments() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n          -----------------\n");

        if (!comments.isEmpty()) {
            for (Comment comment : comments) {
                sb.append("          ").append(comment.getContent()).append(System.lineSeparator());
                sb.append("          -----------------\n");
            }
        } else {
            sb.append("          ===== NO COMMENTS =====\n");
        }

        return sb.toString();
    }

    @Override
    public void addComment(Comment commentToAdd, Task taskToAddComment) {
        taskToAddComment.getComments().add(commentToAdd);
    }

    @Override
    public void removeComment(Comment commentToRemove, Task taskToRemoveComment) {

        taskToRemoveComment.getComments().remove(commentToRemove);
    }

    public abstract String viewInfo();

    @Override
    public String getHistory() {
        StringBuilder builder = new StringBuilder();

        for (EventLog log : history) {
            builder.append(log.viewInfo())
                    .append(System.lineSeparator());
        }
        return builder.toString();
    }

    protected void logEvent(String event) {
        this.history.add(new EventLog(event));
    }

    String basePrint(){
        return "Task ID: %d\n" +
                "Task title: %s\n" +
                "Type: %s\n" +
                "Description: %s\n" +
                "%s Status: %s\n";
    }
}
