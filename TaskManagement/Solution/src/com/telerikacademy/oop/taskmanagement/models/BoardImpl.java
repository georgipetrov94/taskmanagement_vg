package com.telerikacademy.oop.taskmanagement.models;

import com.telerikacademy.oop.taskmanagement.models.common.Validator;
import com.telerikacademy.oop.taskmanagement.models.contracts.Board;

public class BoardImpl extends BaseMemberBoardImpl implements Board {
    private static final int BOARD_NAME_MIN_LENGTH = 5;
    private static final int BOARD_NAME_MAX_LENGTH = 10;
    private static final String BOARD_NAME_INVALID_LENGTH = String.format("Board name should have between %d and %d characters!", BOARD_NAME_MIN_LENGTH, BOARD_NAME_MAX_LENGTH);


    public BoardImpl(String name) {
        super(name);
        Validator.validateIntRange(name.length(), BOARD_NAME_MIN_LENGTH, BOARD_NAME_MAX_LENGTH, BOARD_NAME_INVALID_LENGTH);
        logEvent(String.format("%s", viewInfo()));
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public String viewInfo() {
        return String.format("Board {{%s}} was created", getName());
    }


}
