package com.telerikacademy.oop.taskmanagement.models.contracts;


public interface Task extends Commentable {


    int getTaskId();

    String getTitle();

    String getDescription();

    void addComment(Comment commentToAdd, Task taskToAddComment);

    void removeComment(Comment commentToRemove, Task taskToRemoveComment);

    String showComments();

    String print();

    void advanceStatus();

    void revertStatus();

    String getHistory();


}
