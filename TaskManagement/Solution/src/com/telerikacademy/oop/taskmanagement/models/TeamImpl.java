package com.telerikacademy.oop.taskmanagement.models;

import com.telerikacademy.oop.taskmanagement.models.common.Validator;
import com.telerikacademy.oop.taskmanagement.models.contracts.Board;
import com.telerikacademy.oop.taskmanagement.models.contracts.Person;
import com.telerikacademy.oop.taskmanagement.models.contracts.Team;
import com.telerikacademy.oop.taskmanagement.models.logger.EventLog;


import java.util.ArrayList;
import java.util.List;

public class TeamImpl implements Team {

    private static final int TEAM_MIN_LENGTH = 5;
    private static final int TEAM_MAX_LENGTH = 15;
    private static final String TEAM_INVALID_LENGTH = String.format("Team name should have between %d and %d characters!", TEAM_MIN_LENGTH, TEAM_MAX_LENGTH);


    private final String name;
    private final List<Person> teamPeople = new ArrayList<>();
    private final List<Board> boards = new ArrayList<>();
    private final List<EventLog> history = new ArrayList<>();

    public TeamImpl(String name) {
        Validator.validateIntRange(name.length(), TEAM_MIN_LENGTH, TEAM_MAX_LENGTH, TEAM_INVALID_LENGTH);
        this.name = name;
        logEvent(String.format("%s", viewInfo()));
    }


    @Override
    public List<Board> getBoards() {
        return boards;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String showTeamMembers() {
        StringBuilder str = new StringBuilder();
        if (teamPeople.isEmpty()) {
            return "No people in this team";
        }
        str.append(String.format("People in team %s: ", name));
        str.append("\n------------");
        for (Person person : teamPeople) {
            str.append("\n  ").append(person.getName());
            str.append("\n------------");

        }
        return str.toString();
    }

    @Override
    public void addPerson(Person person) {
        if (teamPeople.contains(person)) {
            throw new IllegalArgumentException(String.format("%s was already added to team %s !", person.getName(), name));
        } else {
            teamPeople.add(person);
            logEvent(String.format("Person with name %s was added to team %s", person.getName(), getName()));
        }
    }

    @Override
    public void addBoard(Board board) {

        boolean isBoardHere = boards.stream().anyMatch(o -> o.getName().equals(board.getName()));

        if (isBoardHere) {
            throw new IllegalArgumentException(String.format("Team %s already has a board with name %s", name, board.getName()));
        } else {
            boards.add(board);
            logEvent(String.format("Board with name %s was added to team %s", board.getName(), getName()));
        }
    }

    @Override
    public String showBoards() {
        StringBuilder str = new StringBuilder();
        if (boards.isEmpty()) {
            return "No boards in this team";
        }
        str.append(String.format("Boards in team %s: ", name));
        str.append("\n------------");
        for (Board str1 : boards) {
            str.append("\n  ").append(str1.getName());
            str.append("\n------------");

        }
        return str.toString();
    }


    @Override
    public String toString() {
        return getName();
    }

    @Override
    public String viewInfo() {
        return String.format("Team {{%s}} was created", getName());
    }


    @Override
    public String getHistory() {
        StringBuilder builder = new StringBuilder();

        for (EventLog log : history) {
            builder.append(log.viewInfo())
                    .append(System.lineSeparator());
        }
        return builder.toString();
    }

    private void logEvent(String event) {
        this.history.add(new EventLog(event));
    }


}
