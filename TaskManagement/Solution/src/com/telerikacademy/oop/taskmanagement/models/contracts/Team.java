package com.telerikacademy.oop.taskmanagement.models.contracts;

import java.util.List;

public interface Team extends Nameable {

    String showTeamMembers();

    void addPerson(Person person);

    void addBoard(Board board);

    String showBoards();

     List<Board> getBoards();

    String viewInfo();

    String getHistory();
}
