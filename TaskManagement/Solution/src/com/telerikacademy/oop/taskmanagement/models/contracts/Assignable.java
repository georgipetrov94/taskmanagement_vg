package com.telerikacademy.oop.taskmanagement.models.contracts;

public interface Assignable extends Task {

    Person getAssignee();

    void setAssignee(Person person);
}
