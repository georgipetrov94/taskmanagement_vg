package com.telerikacademy.oop.taskmanagement.models;

import com.telerikacademy.oop.taskmanagement.models.common.Validator;
import com.telerikacademy.oop.taskmanagement.models.contracts.BaseMemberBoard;
import com.telerikacademy.oop.taskmanagement.models.contracts.Task;
import com.telerikacademy.oop.taskmanagement.models.logger.EventLog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class BaseMemberBoardImpl implements BaseMemberBoard {
    public static final int NAME_MIN_LENGTH = 5;
    public static final int NAME_MAX_LENGTH = 15;
    public static final String NAME_LENGTH_ERROR_MESSAGE = String.format("Name should have between %s and %s chars!", NAME_MIN_LENGTH, NAME_MAX_LENGTH);


    private String name;
    private final HashMap<Integer, Task> tasksList = new HashMap<>();

    private final List<EventLog> history = new ArrayList<>();


    public BaseMemberBoardImpl(String name) {
        setName(name);
    }

    private void setName(String name) {
        Validator.validateIntRange(name.length(), NAME_MIN_LENGTH, NAME_MAX_LENGTH, NAME_LENGTH_ERROR_MESSAGE);
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public HashMap<Integer, Task> getTasks() {
        return tasksList;
    }

    @Override
    public void addTask(Task task) {
        logEvent(String.format("Task %d was added", task.getTaskId()));
        tasksList.put(task.getTaskId(), task);
    }

    @Override
    public void removeTask(Task task) {
        tasksList.remove(task.getTaskId(), task);
        logEvent(String.format("Task %d was removed", task.getTaskId()));
    }

    @Override
    public String getHistory() {
        StringBuilder builder = new StringBuilder();

        for (EventLog log : history) {
            builder.append(log.viewInfo())
                    .append(System.lineSeparator());
        }
        return builder.toString();
    }

    @Override
    public String showTasks() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n          -----------------\n");

        if (!tasksList.isEmpty()) {
            for (Task task : tasksList.values()) {
                sb.append(task.print()).append(System.lineSeparator());
                sb.append("          -----------------\n");
            }
        } else {
            sb.append("          ===== NO TASKS =====\n");
        }

        return sb.toString();
    }

    protected void logEvent(String event) {
        this.history.add(new EventLog(event));
    }

    public abstract String viewInfo();


}
