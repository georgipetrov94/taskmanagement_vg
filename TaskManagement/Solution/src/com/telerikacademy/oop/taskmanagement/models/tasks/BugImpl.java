package com.telerikacademy.oop.taskmanagement.models.tasks;

import com.telerikacademy.oop.taskmanagement.models.PersonImpl;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Priority;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Severity;
import com.telerikacademy.oop.taskmanagement.models.common.enums.BugStatus;
import com.telerikacademy.oop.taskmanagement.models.contracts.Bug;
import com.telerikacademy.oop.taskmanagement.models.contracts.Person;

import java.util.ArrayList;
import java.util.List;

public class BugImpl extends TasksImpl implements Bug {

    private final static String TASK_TYPE = "Bug";

    private final List<String> steps = new ArrayList<>();
    private Priority priority;
    private Severity severity;
    private BugStatus status;
    private Person assignee;


    public BugImpl(String title, String description, Priority priority, Severity severity) {
        super(title, description);
        setPriority(priority);
        setSeverity(severity);
        setStatus(BugStatus.ACTIVE);
        setAssignee(new PersonImpl("Not assigned"));
        logEvent(String.format("Task created: %s", viewInfo()));
    }

    private void setStatus(BugStatus status) {
        this.status = status;
    }

    private void setPriority(Priority priority) {
        this.priority = priority;
    }

    private void setSeverity(Severity severity) {
        this.severity = severity;
    }

    @Override
    public void setAssignee(Person assignee) {
        this.assignee = assignee;
    }

    @Override
    public Person getAssignee() {
        return assignee;
    }

    @Override
    public void removeAssignee(){
        this.assignee = new PersonImpl("Not assigned");
    }

    public List<String> getSteps() {
        return steps;
    }

    public void addStep(String step) {
        steps.add((steps.size() + 1) + ". " + step);
    }

    public Priority getPriority() {
        return priority;
    }

    public Severity getSeverity() {
        return severity;
    }

    public BugStatus getStatus() {
        return status;
    }

    @Override
    public String print() {
        return String.format(basePrint() +
                        "Priority: %s\n" +
                        "Severity: %s\n" +
                        "Assigned to : %s\n" +
                        "Comments : %s\n" +
                        "Steps: %s\n", getTaskId(), getTitle(), TASK_TYPE, getDescription(), TASK_TYPE, getStatus().toString(), getPriority().toString(), getSeverity().toString(),
                getAssignee(), showComments(), getSteps());

    }

    @Override
    public String viewInfo() {
        return String.format("Bug {{%s}} was created", getTitle());
    }

    @Override
    public void advanceStatus() {
        if (status != BugStatus.FIXED) {
            logEvent(String.format("Bug status changed from %s to %s", getStatus(), BugStatus.values()[status.ordinal() + 1]));
            setStatus(BugStatus.values()[status.ordinal() + 1]);
            System.out.println("Status changed");
        } else {
            System.out.printf("Can't advance, already at %s%n", getStatus());
        }
    }

    @Override
    public void revertStatus() {
        if (status != BugStatus.ACTIVE) {
            logEvent(String.format("Bug status changed from %s to %s", getStatus(), BugStatus.values()[status.ordinal() - 1]));
            setStatus(BugStatus.values()[status.ordinal() - 1]);
            System.out.println("Status changed");
        } else {
            System.out.printf("Can't revert, already at %s%n", getStatus());
        }
    }

    @Override
    public void advancePriority() {
        if (priority != Priority.HIGH) {
            logEvent(String.format("Bug priority changed from %s to %s", getPriority(), Priority.values()[priority.ordinal() + 1]));
            setPriority(Priority.values()[priority.ordinal() + 1]);
            System.out.println("Priority changed");
        } else {
            System.out.printf("Can't advance, already at %s%n", getPriority());
        }
    }

    @Override
    public void revertPriority() {
        if (priority != Priority.LOW) {
            logEvent(String.format("Bug priority changed from %s to %s", getPriority(), Priority.values()[priority.ordinal() - 1]));
            setPriority(Priority.values()[priority.ordinal() - 1]);
            System.out.println("Priority changed");
        } else {
            System.out.printf("Can't revert, already at %s%n", getPriority());
        }
    }

    @Override
    public void advanceSeverity() {
        if (severity != Severity.CRITICAL) {
            logEvent(String.format("Bug severity changed from %s to %s", getSeverity(), Severity.values()[severity.ordinal() + 1]));
            setSeverity(Severity.values()[severity.ordinal() + 1]);
            System.out.println("Severity changed");
        } else {
            System.out.printf("Can't advance, already at %s%n", getSeverity());
        }
    }

    @Override
    public void revertSeverity() {
        if (severity != Severity.MINOR) {
            logEvent(String.format("Bug severity changed from %s to %s", getSeverity(), Severity.values()[severity.ordinal() - 1]));
            setSeverity(Severity.values()[severity.ordinal() - 1]);
            System.out.println("Severity changed");
        } else {
            System.out.printf("Can't revert, already at %s%n", getSeverity());
        }
    }


}