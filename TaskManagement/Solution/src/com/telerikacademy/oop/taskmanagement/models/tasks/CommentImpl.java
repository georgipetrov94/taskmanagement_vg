package com.telerikacademy.oop.taskmanagement.models.tasks;

import com.telerikacademy.oop.taskmanagement.models.common.Validator;
import com.telerikacademy.oop.taskmanagement.models.contracts.Comment;

public class CommentImpl implements Comment {
    private String author;
    private String content;

    public CommentImpl(String content, String author) {
        setContent(content);
        setAuthor(author);
    }

    private void setContent(String content) {
        this.content = content;
    }

    private void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    @Override
    public String getContent() {
        return content;
    }


}
