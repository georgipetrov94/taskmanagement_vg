package com.telerikacademy.oop.taskmanagement.models.contracts;

import java.util.HashMap;

public interface BaseMemberBoard {

    public String getName();

    void addTask(Task task);

    void removeTask(Task task);

    HashMap<Integer, Task> getTasks();

    String showTasks();

    String getHistory();

}
