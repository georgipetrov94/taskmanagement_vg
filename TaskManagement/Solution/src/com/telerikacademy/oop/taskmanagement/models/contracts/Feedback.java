package com.telerikacademy.oop.taskmanagement.models.contracts;

import com.telerikacademy.oop.taskmanagement.models.common.enums.FeedbackStatus;


public interface Feedback extends Task {


    int getRating();

    FeedbackStatus getStatus();

    void changeRating(int i);
}
