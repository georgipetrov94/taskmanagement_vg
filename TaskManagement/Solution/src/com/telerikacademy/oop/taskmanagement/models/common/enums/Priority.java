package com.telerikacademy.oop.taskmanagement.models.common.enums;


public enum Priority {
    LOW, MEDIUM, HIGH;

    @Override
    public String toString() {
        switch (this) {
            case LOW:
                return "Low";
            case MEDIUM:
                return "Medium";
            case HIGH:
                return "High";
            default:
                return "";
        }
    }


}
