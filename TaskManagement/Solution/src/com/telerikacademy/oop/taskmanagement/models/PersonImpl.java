package com.telerikacademy.oop.taskmanagement.models;



import com.telerikacademy.oop.taskmanagement.models.contracts.Person;



public class PersonImpl extends BaseMemberBoardImpl implements Person {


    public PersonImpl(String name) {
        super(name);
        logEvent(String.format("%s", viewInfo()));
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public String viewInfo() {
        return String.format("Member {{%s}} was created", getName());
    }

}
