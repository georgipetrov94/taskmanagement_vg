package com.telerikacademy.oop.taskmanagement.models.contracts;

public interface Nameable {

    String getName();

}
