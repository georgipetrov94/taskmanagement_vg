package com.telerikacademy.oop.taskmanagement.models.contracts;

import com.telerikacademy.oop.taskmanagement.models.common.enums.Priority;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Size;
import com.telerikacademy.oop.taskmanagement.models.common.enums.StoryStatus;


public interface Story extends Assignable {


    StoryStatus getStatus();

    Priority getPriority();

    Size getSize();

    void advancePriority();

    void revertPriority();

    void advanceSize();

    void revertSize();

    void setAssignee(Person assignee);

    Person getAssignee();

    void removeAssignee();
}
