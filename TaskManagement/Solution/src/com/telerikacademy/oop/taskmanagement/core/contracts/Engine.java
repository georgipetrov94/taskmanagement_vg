package com.telerikacademy.oop.taskmanagement.core.contracts;

public interface Engine {
    
    void start();
    
}
