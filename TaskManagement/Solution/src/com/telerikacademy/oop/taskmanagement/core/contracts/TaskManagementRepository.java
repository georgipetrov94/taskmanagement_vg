package com.telerikacademy.oop.taskmanagement.core.contracts;

import com.telerikacademy.oop.taskmanagement.models.contracts.*;

import java.util.HashMap;
import java.util.List;

public interface TaskManagementRepository {

    List<Person> getPeople();

    void addPerson(Person personToAdd);

    void addTeam(Team teamToAdd);

    List<Team> getTeams();

    HashMap<Integer, Bug> getBugs();

    void addBug(Bug task);

    HashMap<Integer, Feedback> getFeedbacks();

    void addFeedback(Feedback task);

    void addStory(Story task);

    HashMap<Integer, Story> getStories();

    HashMap<Integer, Task> getTasks();

    HashMap<Integer, Assignable> getAssignables();


}
