package com.telerikacademy.oop.taskmanagement.core.contracts;

import com.telerikacademy.oop.taskmanagement.models.TeamImpl;
import com.telerikacademy.oop.taskmanagement.models.contracts.*;

public interface TaskManagementFactory {

    Person createPerson(String name);

    TeamImpl createTeam(String name);

    Comment createComment(String content, String author);



}
