package com.telerikacademy.oop.taskmanagement.core.factories;

import com.telerikacademy.oop.taskmanagement.commands.*;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.commands.enums.CommandType;
import com.telerikacademy.oop.taskmanagement.core.contracts.CommandFactory;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementFactory;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;

public class CommandFactoryImpl implements CommandFactory {

    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    @Override
    public Command createCommand(String commandTypeAsString, TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        CommandType commandType = CommandType.valueOf(commandTypeAsString.toUpperCase());
        switch (commandType) {
            case CREATEPERSON:
                return new AddPerson(taskManagementFactory, taskManagementRepository);
            case SHOWPEOPLE:
                return new ShowPeople(taskManagementRepository);
            case CREATETEAM:
                return new AddTeam(taskManagementFactory, taskManagementRepository);
            case SHOWTEAMS:
                return new ShowTeams(taskManagementRepository);
            case SHOWTEAMMEMBERS:
                return new ShowTeamMembers(taskManagementRepository);
            case ADDPERSONINTEAM:
                return new AddPersonTeam(taskManagementRepository);
            case COMMANDS:
                return new Commands();
            case CREATEBOARD:
                return new AddBoard(taskManagementRepository);
            case SHOWBOARDS:
                return new ShowBoards(taskManagementRepository);
            case CREATEBUG:
                return new AddBug(taskManagementRepository);
            case SHOWTASKS:
                return new ShowTasks(taskManagementRepository);
            case SHOWTASKID:
                return new ShowTaskId(taskManagementRepository);
            case CREATESTORY:
                return new AddStory(taskManagementRepository);
            case CREATEFEEDBACK:
                return new AddFeedback(taskManagementRepository);
            case ASSIGNTASKTOPERSON:
                return new AssignTaskToPerson(taskManagementRepository);
            case SHOWTASKTYPE:
                return new ShowTypeTask(taskManagementRepository);
            case ADDCOMMENT:
                return new AddComment(taskManagementFactory, taskManagementRepository);
            case SHOWPERSONTASKS:
                return new ShowPersonTasks(taskManagementRepository);
            case TASKCHANGE:
                return new TaskChange(taskManagementRepository);
            case UNASSIGNTASK:
                return new UnassignTask(taskManagementRepository);
            case SHOWTEAMACTIVITY:
                return new GetTeamActivity(taskManagementRepository);
            case SHOWPERSONACTIVITY:
                return new GetPersonActivity(taskManagementRepository);
            case SHOWBOARDACTIVITY:
                return new GetBoardActivity(taskManagementRepository);
            case SORTTITLE:
                return new SortingTitle(taskManagementRepository);
            case SORTSTATUS:
                return new SortStatusTasks(taskManagementRepository);
            case SORTPRIORITY:
                return new SortPriorityBugsStories(taskManagementRepository);
            case SORTSTORYSIZE:
                return new SortSizeStories(taskManagementRepository);
            case SORTFEEDBACKRATING:
                return new SortRatingFeedbacks(taskManagementRepository);
            case SORTBUGSEVERITY:
                return new SortSeverityBugs(taskManagementRepository);
            case ADDBUGSTEPS:
                return new AddBugSteps(taskManagementRepository);
            case SHOWTASKACTIVITY:
                return new GetTaskActivity(taskManagementRepository);
            case ASSIGNTASKTOBOARD:
                return new AssignTaskToBoard(taskManagementRepository);
            case SHOWBOARDTASKS:
                return new ShowBoardTasks(taskManagementRepository);


        }
        throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandTypeAsString));
    }

}
