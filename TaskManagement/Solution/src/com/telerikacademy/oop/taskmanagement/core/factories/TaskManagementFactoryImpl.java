package com.telerikacademy.oop.taskmanagement.core.factories;

import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementFactory;
import com.telerikacademy.oop.taskmanagement.models.PersonImpl;
import com.telerikacademy.oop.taskmanagement.models.TeamImpl;
import com.telerikacademy.oop.taskmanagement.models.contracts.Comment;

import com.telerikacademy.oop.taskmanagement.models.tasks.CommentImpl;



public class TaskManagementFactoryImpl implements TaskManagementFactory {

    @Override
    public PersonImpl createPerson(String name) {
        return new PersonImpl(name);
    }

    @Override
    public TeamImpl createTeam(String name) {
        return new TeamImpl(name);
    }

    @Override
    public Comment createComment(String content, String author) {
        return new CommentImpl(content, author);
    }


}