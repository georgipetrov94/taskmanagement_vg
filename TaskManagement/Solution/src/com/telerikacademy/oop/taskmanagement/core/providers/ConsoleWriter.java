package com.telerikacademy.oop.taskmanagement.core.providers;

import com.telerikacademy.oop.taskmanagement.core.contracts.Writer;

public class ConsoleWriter implements Writer {
    
    public void writeLine(String message) {
        System.out.println(message);
    }
    
}
