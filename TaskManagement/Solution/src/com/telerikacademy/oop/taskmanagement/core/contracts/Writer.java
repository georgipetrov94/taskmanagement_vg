package com.telerikacademy.oop.taskmanagement.core.contracts;

public interface Writer {
    
    void writeLine(String message);
    
}
