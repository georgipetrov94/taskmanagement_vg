package com.telerikacademy.oop.taskmanagement.core.contracts;

public interface Reader {
    
    String readLine();
    
}
