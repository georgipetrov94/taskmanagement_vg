package com.telerikacademy.oop.taskmanagement.core;

import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.*;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TaskManagementRepositoryImpl implements TaskManagementRepository {

    private static final String NAME_EXISTS_EXCEPTION = "Name %s already exists!";

    private final List<Person> people = new ArrayList<>();
    private final List<Team> teams = new ArrayList<>();
    private final HashMap<Integer, Bug> bugsList = new HashMap<>();
    private final HashMap<Integer, Feedback> feedbackList = new HashMap<>();
    private final HashMap<Integer, Story> storiesList = new HashMap<>();


    public TaskManagementRepositoryImpl() {
    }



    public HashMap<Integer, Task> getTasks() {
        var map=new HashMap<Integer, Task>();
        map.putAll(bugsList);
        map.putAll(feedbackList);
        map.putAll(storiesList);

        return map;
    }

    public HashMap<Integer, Assignable> getAssignables() {
        var map=new HashMap<Integer, Assignable>();
        map.putAll(bugsList);
        map.putAll(storiesList);

        return map;
    }

    public void addStory(Story task) {
        storiesList.put(task.getTaskId(), task);
    }

    public HashMap<Integer, Story> getStories() {
        return storiesList;
    }

    public void addFeedback(Feedback task) {
        feedbackList.put(task.getTaskId(), task);

    }

    public HashMap<Integer, Feedback> getFeedbacks() {
        return feedbackList;
    }

    public void addBug(Bug task) {
        bugsList.put(task.getTaskId(), task);
    }

    public HashMap<Integer, Bug> getBugs() {
        return bugsList;
    }


    public List<Person> getPeople() {
        return new ArrayList<>(people);
    }


    public void addTeam(Team teamToAdd) {
        checkUniqueName(teams, teamToAdd.toString());
        teams.add(teamToAdd);
    }


    public void addPerson(Person personToAdd) {
        checkUniqueName(people, personToAdd.toString());
        people.add(personToAdd);

    }

    @Override
    public List<Team> getTeams() {
        return new ArrayList<>(teams);
    }

    private <E extends Nameable> void checkUniqueName(List<E> items, String name) {
        var exists = items.stream().anyMatch(item -> item.getName().equalsIgnoreCase(name));

        if (exists) {
            throw new IllegalArgumentException(NAME_EXISTS_EXCEPTION);
        }
    }

}
