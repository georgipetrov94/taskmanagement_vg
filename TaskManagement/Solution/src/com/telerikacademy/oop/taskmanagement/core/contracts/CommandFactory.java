package com.telerikacademy.oop.taskmanagement.core.contracts;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;

public interface CommandFactory {
    
    Command createCommand(String commandTypeAsString, TaskManagementFactory dealershipFactory, TaskManagementRepository dealershipRepository);
    
}
