package com.telerikacademy.oop.taskmanagement.core;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.factories.CommandFactoryImpl;
import com.telerikacademy.oop.taskmanagement.core.factories.TaskManagementFactoryImpl;
import com.telerikacademy.oop.taskmanagement.core.providers.CommandParserImpl;
import com.telerikacademy.oop.taskmanagement.core.providers.ConsoleReader;
import com.telerikacademy.oop.taskmanagement.core.providers.ConsoleWriter;
import com.telerikacademy.oop.taskmanagement.core.contracts.*;

import java.util.List;

public class TaskManagementEngineImpl implements Engine {
    
    private static final String TERMINATION_COMMAND = "Exit";
    private static final String REPORT_SEPARATOR = "####################";
    
    private final TaskManagementFactory taskManagementFactory;
    private final Writer writer;
    private final Reader reader;
    private final CommandParser commandParser;
    private final TaskManagementRepository taskManagementRepository;
    private final CommandFactory commandFactory;
    
    public TaskManagementEngineImpl() {
        taskManagementFactory = new TaskManagementFactoryImpl();
        writer = new ConsoleWriter();
        reader = new ConsoleReader();
        commandParser = new CommandParserImpl();
        taskManagementRepository = new TaskManagementRepositoryImpl();
        commandFactory = new CommandFactoryImpl();
    }
    
    public void start() {
        
        while (true) {
            try {
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(commandAsString);
                
            } catch (Exception ex) {
                writer.writeLine(ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());
                writer.writeLine(REPORT_SEPARATOR);
            }
        }
    }
    
    private void processCommand(String commandAsString) {
        if (commandAsString == null || commandAsString.trim().equals("")) {
            throw new IllegalArgumentException("Command cannot be null or empty.");
        }
        
        String commandTypeAsString = commandParser.parseCommand(commandAsString);
        Command command = commandFactory.createCommand(commandTypeAsString, taskManagementFactory, taskManagementRepository);
        List<String> parameters = commandParser.parseParameters(commandAsString);
        String executionResult = command.execute(parameters).replace("_"," ");
        writer.writeLine(executionResult);
        writer.writeLine(REPORT_SEPARATOR);
        
    }
    

    
}
