package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Feedback;
import com.telerikacademy.oop.taskmanagement.models.contracts.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortRatingFeedbacks implements Command {


    private final TaskManagementRepository taskManagementRepository;

    public SortRatingFeedbacks(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }


    @Override
    public String execute(List<String> parameters) {

        return sortRatingFeedbacks();

    }

    private String sortRatingFeedbacks() {

        List<Feedback> list2 = new ArrayList<>(taskManagementRepository.getFeedbacks().values());

        return "Sorted by rating Feedbacks#########\n"
                + list2.stream()
                .sorted(Comparator.comparing(Feedback::getRating))
                .map(Task::print)
                .collect(Collectors.joining());

    }

}
