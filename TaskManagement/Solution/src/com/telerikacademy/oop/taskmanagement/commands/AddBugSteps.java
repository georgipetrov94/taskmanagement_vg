package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;

import java.util.List;
import java.util.Scanner;

public class AddBugSteps implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    private static final String WRONG_PARAMETERS_EXCEPTION = "Wrong parameters for task ID or/and steps number";
    private static final String INVALID_TASK_TYPE_EXCEPTION = "You can't add steps here!";

    private final TaskManagementRepository taskManagementRepository;

    public AddBugSteps(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        int taskID;
        int stepsNumber;
        try {
            taskID = Integer.parseInt(parameters.get(0));
            stepsNumber = Integer.parseInt(parameters.get(1));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(WRONG_PARAMETERS_EXCEPTION);
        }

        return addBugSteps(taskID, stepsNumber);

    }

    private String addBugSteps(int taskID, int stepsNumber) {

        Scanner sc = new Scanner(System.in);
        if (taskManagementRepository.getBugs().containsKey(taskID)) {
            for (int i = 0; i < stepsNumber; i++) {
                System.out.println("Add step #" + (i + 1));
                String step = sc.nextLine();
                taskManagementRepository.getBugs().get(taskID).addStep(step);
            }
        } else {
            throw new IllegalArgumentException(INVALID_TASK_TYPE_EXCEPTION);
        }

        return "";
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS,
                            parameters.size()));
        }
    }


}
