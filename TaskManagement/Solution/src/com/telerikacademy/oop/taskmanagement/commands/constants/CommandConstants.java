package com.telerikacademy.oop.taskmanagement.commands.constants;

public class CommandConstants {

    public static final String INVALID_COMMAND = "Invalid command! %s";

    public static final String USER_ALREADY_EXIST = "User %s already exist. Choose a different username!";
    public static final String USER_LOGGED_IN_ALREADY = "User %s is logged in! Please log out first!";
    public static final String USER_REGISTERED = "User %s registered successfully!";
    public static final String NO_SUCH_PERSON = "There is no person with name %s!";
    public static final String USER_LOGGED_OUT = "You logged out!";
    public static final String USER_LOGGED_IN = "User %s successfully logged in!";
    public static final String WRONG_USERNAME_OR_PASSWORD = "Wrong username or password!";
    public static final String TASK_UNASSIGNED = "Task with ID %d was unassigned from %s !";

    public static final String TASK_ASSIGNED = "Task with ID %d was assigned to %s !";
    public static final String TASK_ASSIGNED_BOARD = "Task with ID %d was assigned to %s in team %s!";
    public static final String NEW_TASK_ADDED = "New task with ID %d was added !";

    public static final String NEW_TEAM_ADDED = "New team with name %s was added !";
    public static final String NEW_PERSON_ADDED = "New person with name %s was added !";

    public static final String NEW_MEMBER_ADDED = "%s was added to team %s !";
    public static final String NEW_BOARD_ADDED = "Board with name %s added to team %s !";

    public static final String COMMENT_DOES_NOT_EXIST = "The comment does not exist!";
    public static final String COMMENT_ADDED_SUCCESSFULLY = "%s added comment successfully!";
    public static final String VEHICLE_DOES_NOT_EXIST = "The vehicle does not exist!";
    public static final String REPORT_SEPARATOR = "####################";
    public static final int NUMBER_OF_PARAMETERS_FOR_NORMAL_ROLE = 4;

}
