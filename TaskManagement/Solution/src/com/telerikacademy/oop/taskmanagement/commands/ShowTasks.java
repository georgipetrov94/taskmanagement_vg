package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.*;

import java.util.List;

public class ShowTasks implements Command {

    private final TaskManagementRepository taskManagementRepository;

    public ShowTasks(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        return showAllTasks();

    }

    private String showAllTasks() {

        StringBuilder str = new StringBuilder("All Tasks#########\n");

        for (Task task : taskManagementRepository.getTasks().values()) {
            str.append(taskManagementRepository.getTasks().get(task.getTaskId()).print());
            str.append(System.lineSeparator());
        }

        return str.toString();
    }
}
