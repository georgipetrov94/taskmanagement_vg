package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.constants.CommandConstants;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Bug;
import com.telerikacademy.oop.taskmanagement.models.contracts.Person;
import com.telerikacademy.oop.taskmanagement.models.contracts.Story;

import java.util.List;

public class UnassignTask implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    private static final String WRONG_PARAMETERS_EXCEPTION = "Wrong parameters for Task ID";
    private static final String NO_PERSON_WITH_NAME_EXCEPTION = "No person with name %s";
    private static final String NO_TASK_WITH_ID_EXCEPTION = "No task with ID %d or unable to unassign!";


    private final TaskManagementRepository taskManagementRepository;

    public UnassignTask(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        String person = parameters.get(0);
        int taskID;
        try {
            taskID = Integer.parseInt(parameters.get(1));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(WRONG_PARAMETERS_EXCEPTION);
        }

        return unassignTask(person, taskID);
    }


    private String unassignTask(String person, int taskID) {

        Person person1 = taskManagementRepository.getPeople()
                .stream()
                .filter(u -> u.getName().equalsIgnoreCase(person))
                .findFirst().orElseThrow(() -> new IllegalArgumentException(String.format(NO_PERSON_WITH_NAME_EXCEPTION, person)));

        Bug getBugTask = taskManagementRepository.getBugs().get(taskID);
        Story getStoryTask = taskManagementRepository.getStories().get(taskID);

        if (taskManagementRepository.getBugs().containsKey(taskID)) {

            person1.removeTask(getBugTask);
            getBugTask.removeAssignee();

        } else if (taskManagementRepository.getStories().containsKey(taskID)) {

            person1.removeTask(getStoryTask);
            getStoryTask.removeAssignee();

        } else {
            throw new IllegalArgumentException(String.format(NO_TASK_WITH_ID_EXCEPTION, taskID));
        }


        return String.format(CommandConstants.TASK_UNASSIGNED, taskID, person1.getName());
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS,
                            parameters.size()));
        }
    }

}
