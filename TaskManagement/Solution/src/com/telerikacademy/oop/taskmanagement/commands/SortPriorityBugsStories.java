package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Bug;
import com.telerikacademy.oop.taskmanagement.models.contracts.Story;
import com.telerikacademy.oop.taskmanagement.models.contracts.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortPriorityBugsStories implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    private static final String INVALID_TYPE_TASK_NAME = "That task type doesn't have priority/Invalid type task name !";


    private final TaskManagementRepository taskManagementRepository;

    public SortPriorityBugsStories(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        String typeTask = parameters.get(0);

        return sortPriorityTasks(typeTask);

    }

    private String sortPriorityTasks(String typeTask) {

        switch (typeTask.toUpperCase()) {
            case "BUGS":
                List<Bug> list1 = new ArrayList<>(taskManagementRepository.getBugs().values());

                return "Sorted by priority Bugs#########\n"
                        + list1.stream()
                        .sorted(Comparator.comparing(Bug::getPriority))
                        .map(Task::print)
                        .collect(Collectors.joining());

            case "STORIES":
                List<Story> list3 = new ArrayList<>(taskManagementRepository.getStories().values());

                return "Sorted by priority Stories#########\n"
                        + list3.stream()
                        .sorted(Comparator.comparing(Story::getPriority))
                        .map(Task::print)
                        .collect(Collectors.joining());

            default:
                throw new IllegalArgumentException(INVALID_TYPE_TASK_NAME);
        }
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS,
                            parameters.size()));
        }
    }

}
