package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.constants.CommandConstants;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementFactory;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Comment;
import com.telerikacademy.oop.taskmanagement.models.contracts.Person;

import java.util.List;

public class AddComment implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    private static final String WRONG_PARAMETERS_EXCEPTION = "Wrong parameter for task ID";
    private static final String NO_TASK_WITH_ID_EXCEPTION = "No task with ID %d";

    private final TaskManagementFactory taskManagementFactory;
    private final TaskManagementRepository taskManagementRepository;

    public AddComment(TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        this.taskManagementFactory = taskManagementFactory;
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        String content = parameters.get(0);
        int id;
        try {
            id = Integer.parseInt(parameters.get(1));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(WRONG_PARAMETERS_EXCEPTION);
        }
        String author = parameters.get(2);

        return addComment(content, id, author);
    }

    private String addComment(String content, int id, String author) {
        Comment comment = taskManagementFactory.createComment(content, author);
        Person person1 = taskManagementRepository.getPeople()
                .stream()
                .filter(u -> u.getName().equalsIgnoreCase(author))
                .findFirst().orElseThrow(() -> new IllegalArgumentException(String.format(CommandConstants.NO_SUCH_PERSON, author)));

        if (taskManagementRepository.getTasks().containsKey(id)) {
            taskManagementRepository.getTasks().get(id).addComment(comment, taskManagementRepository.getTasks().get(id));
        } else {
            throw new IllegalArgumentException(String.format(NO_TASK_WITH_ID_EXCEPTION, id));
        }
        return String.format(CommandConstants.COMMENT_ADDED_SUCCESSFULLY, author);
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS,
                            parameters.size()));
        }
    }

}
