package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Bug;
import com.telerikacademy.oop.taskmanagement.models.contracts.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortSeverityBugs implements Command {


    private final TaskManagementRepository taskManagementRepository;

    public SortSeverityBugs(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        return sortSeverityTasks();

    }

    private String sortSeverityTasks() {

        List<Bug> list1 = new ArrayList<>(taskManagementRepository.getBugs().values());

        return "Sorted by severity Bugs#########\n"
                + list1.stream()
                .sorted(Comparator.comparing(Bug::getSeverity))
                .map(Task::print)
                .collect(Collectors.joining());
    }

}

