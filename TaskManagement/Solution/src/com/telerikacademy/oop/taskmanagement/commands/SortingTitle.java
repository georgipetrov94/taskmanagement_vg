package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Story;
import com.telerikacademy.oop.taskmanagement.models.contracts.Task;

import java.util.*;
import java.util.stream.Collectors;

public class SortingTitle implements Command {


    private final TaskManagementRepository taskManagementRepository;

    public SortingTitle(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        return sortTitleTasks();

    }

    private String sortTitleTasks() {
        List<Task> list = new ArrayList<>(taskManagementRepository.getTasks().values());

        return "Sorted by title Tasks#########\n"
                + list.stream()
                .sorted(Comparator.comparing(Task::getTitle))
                .map(Task::print)
                .collect(Collectors.joining());

    }

}
