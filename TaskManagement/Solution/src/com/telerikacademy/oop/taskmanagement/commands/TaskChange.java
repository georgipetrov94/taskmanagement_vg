package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Bug;
import com.telerikacademy.oop.taskmanagement.models.contracts.Feedback;
import com.telerikacademy.oop.taskmanagement.models.contracts.Story;

import java.util.List;

public class TaskChange implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS_FEEDBACK = 3;
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS_BUGSTORY = 2;
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    private static final String WRONG_PARAMETERS_EXCEPTION = "Wrong parameters for Task ID or/and Size!";
    private static final String PRIORITY_EXCEPTION = "You can not change priority on this task !";
    private static final String SEVERITY_EXCEPTION = "You can not change severity on this task !";
    private static final String SIZE_EXCEPTION = "You can not change size on this task !";
    private static final String RATING_EXCEPTION = "You can not change rating on this task !";
    private static final String NO_SUCH_COMMAND_EXCEPTION = "No such command !";
    private static final String NO_SUCH_TASK_ID_EXCEPTION = "No such task ID !";


    private final TaskManagementRepository taskManagementRepository;

    public TaskChange(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        if (parameters.size() <= 2) {
            parameters.add("0");
        }
        int taskID;
        int size;
        try {
            taskID = Integer.parseInt(parameters.get(0));
            size = Integer.parseInt(parameters.get(2));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(WRONG_PARAMETERS_EXCEPTION);
        }

        String change = parameters.get(1);


        return showTask(taskID, change, size);
    }

    private String showTask(int taskID, String change, int size) {

        if (taskManagementRepository.getTasks().containsKey(taskID)) {

            switch (change.toUpperCase()) {
                case "ADVANCESTATUS":

                    taskManagementRepository.getTasks().get(taskID).advanceStatus();
                    break;

                case "REVERTSTATUS":

                    taskManagementRepository.getTasks().get(taskID).revertStatus();
                    break;

                case "ADVANCEPRIORITY":

                    if (isBug(taskID)) {
                        (getBug(taskID)).advancePriority();
                    } else if (isStory(taskID)) {
                        getStory(taskID).advancePriority();
                    } else {
                        throw new IllegalArgumentException(PRIORITY_EXCEPTION);
                    }
                    break;

                case "REVERTPRIORITY":

                    if (isBug(taskID)) {
                        getBug(taskID).revertPriority();
                    } else if (isStory(taskID)) {
                        getStory(taskID).revertPriority();
                    } else {
                        throw new IllegalArgumentException(PRIORITY_EXCEPTION);
                    }
                    break;

                case "ADVANCESEVERITY":

                    if (isBug(taskID)) {
                        getBug(taskID).advanceSeverity();
                    } else {
                        throw new IllegalArgumentException(SEVERITY_EXCEPTION);
                    }
                    break;

                case "REVERTSEVERITY":

                    if (isBug(taskID)) {
                        getBug(taskID).revertSeverity();
                    } else {
                        throw new IllegalArgumentException(SEVERITY_EXCEPTION);
                    }
                    break;

                case "ADVANCESIZE":

                    if (isStory(taskID)) {
                        getStory(taskID).advanceSize();
                    } else {
                        throw new IllegalArgumentException(SIZE_EXCEPTION);
                    }
                    break;

                case "REVERTSIZE":

                    if (isStory(taskID)) {
                        getStory(taskID).revertSize();
                    } else {
                        throw new IllegalArgumentException(SIZE_EXCEPTION);
                    }
                    break;

                case "CHANGERATING":

                    if (isFeedback(taskID)) {
                        getFeedback(taskID).changeRating(size);
                    } else {
                        throw new IllegalArgumentException(RATING_EXCEPTION);
                    }
                    break;

                default:
                    throw new IllegalArgumentException(NO_SUCH_COMMAND_EXCEPTION);
            }
        } else {
            throw new IllegalArgumentException(NO_SUCH_TASK_ID_EXCEPTION);
        }


        return "------------\n";
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS_FEEDBACK && parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS_BUGSTORY) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS_FEEDBACK,
                            parameters.size()));
        }
    }


    private Feedback getFeedback(int taskID) {
        return taskManagementRepository.getFeedbacks().get(taskID);
    }

    private Story getStory(int taskID) {
        return taskManagementRepository.getStories().get(taskID);
    }

    private Bug getBug(int taskID) {
        return taskManagementRepository.getBugs().get(taskID);
    }

    private boolean isFeedback(int taskID) {
        return taskManagementRepository.getFeedbacks().containsKey(taskID);
    }

    private boolean isStory(int taskID) {
        return taskManagementRepository.getStories().containsKey(taskID);
    }

    private boolean isBug(int taskID) {
        return taskManagementRepository.getBugs().containsKey(taskID);
    }
}

