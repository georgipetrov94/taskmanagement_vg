package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Team;

import java.util.List;

public class ShowTeamMembers implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    private static final String NO_TEAM_WITH_NAME_EXCEPTION = "No team with name %s";


    private final TaskManagementRepository taskManagementRepository;

    public ShowTeamMembers(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        String team = parameters.get(0);

        return showTeamMember(team);
    }

    private String showTeamMember(String name) {

        Team team1 = taskManagementRepository.getTeams()
                .stream()
                .filter(u -> u.getName().equalsIgnoreCase(name))
                .findFirst().orElseThrow(() -> new IllegalArgumentException(String.format(NO_TEAM_WITH_NAME_EXCEPTION, name)));

        return team1.showTeamMembers();
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS,
                            parameters.size()));
        }
    }


}
