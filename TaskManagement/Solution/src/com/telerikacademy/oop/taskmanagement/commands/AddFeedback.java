package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.constants.CommandConstants;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Feedback;
import com.telerikacademy.oop.taskmanagement.models.tasks.FeedbackImpl;

import java.util.List;

public class AddFeedback implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    private static final String WRONG_PARAMETERS_EXCEPTION = "Wrong parameter for rating !";


    private final TaskManagementRepository taskManagementRepository;

    public AddFeedback(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        String name = parameters.get(0);
        String description = parameters.get(1);
        String rating = parameters.get(2);

        return addFeedback(name, description, rating);
    }


    private String addFeedback(String name, String description, String rating) {

        Feedback feedback;
        try {
            feedback = new FeedbackImpl(name, description, Integer.parseInt(rating));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(WRONG_PARAMETERS_EXCEPTION);
        }

        taskManagementRepository.addFeedback(feedback);

        return String.format(CommandConstants.NEW_TASK_ADDED, feedback.getTaskId());
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS,
                            parameters.size()));
        }
    }


}
