package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;

import java.util.List;

public class Commands implements Command {


    @Override
    public String execute(List<String> parameters) {
        return showAllCommands();
    }

    private String showAllCommands() {

        String str = "1.createperson (Name)                                                    - Creates Person" +
                "\n" + "2.createteam  (Name)                                                     - Creates Team and a Base Board in it, where all Team Tasks are stored" +
                "\n" + "3.showteams                                                              - Shows all teams" +
                "\n" + "4.showpeople                                                             - Shows all people" +
                "\n" + "5.addpersoninteam  (Team name) (Person name)                             - Adds Person to Team" +
                "\n" + "6.showteammembers  (Team name)                                           - Shows all people in a team" +
                "\n" + "7.createboard  (Team name) (Board name)                                  - Creates board in a team" +
                "\n" + "8.showboards  (Team name)                                                - Shows all boards in a team" +
                "\n" + "9.createbug  (Bug name) (Description) (Priority) (Severity               - Creates a Bug and stores it in a Board in Team" +
                "\n" + "10.showtasks                                                             - Shows all Tasks" +
                "\n" + "11.showtaskid (TaskID)                                                   - Gets the TaskID and shows description for the Task" +
                "\n" + "12.createstory (Story name) (Description) (Priority) (Size)              - Creates a Story and stores it in a Board in Team" +
                "\n" + "13.createfeedbacks  (Feedback name) (Description) (Rating)               - Created a Feedback and stores it in a Board in Team" +
                "\n" + "14.assigntasktoperson (Person name)(Task ID)                             - Assign a task to a person" +
                "\n" + "15.showtasktype (feedbacks/stories/bugs)                                 - Shows all Tasks from the given Type" +
                "\n" + "16.addcomment (content) (task) (author)                                  - Adds a comment to a task" +
                "\n" + "17.showpersontasks (Person)                                              - Shows all tasks assigned to Person" +
                "\n" + "18.unassigntask (Person name) (Task ID)                                  - Unassign a task from a person" +
                "\n" + "19.showteamactivity (Team name)                                          - Shows team activity" +
                "\n" + "20.showpersonactivity (Person name)                                      - Shows person activity" +
                "\n" + "21.showboardactivity (Team name)(Board name)                             - Shows board activity" +
                "\n" + "22.taskchange (Task ID) (advance/revert status,priority,severity,size," +
                "\n" + "changerating) (in case of changerating put number)                       - Changes task properties" +
                "\n" + "23.addbugsteps (Task ID) (Number of steps)                               - Adds steps for bugs" +
                "\n" + "24.showtaskactivity (Task ID)                                            - Shows task activity" +
                "\n" + "25.sortstatus (Task type)                                                - Sorts type tasks by status" +
                "\n" + "26.sorttitle                                                             - Sorts all tasks by title" +
                "\n" + "27.sortpriority (Type task)                                              - Sorts tasks(only bugs and stories) by priority" +
                "\n" + "28.sortstorysize                                                         - Sorts stories by size" +
                "\n" + "29.sortfeedbackrating                                                    - Sorts feedbacks by rating" +
                "\n" + "30.sortbugseverity                                                       - Sorts bugs by severity" +
                "\n" + "31.addbugsteps (Task ID) (Number of steps)                               - Lets you add number of steps to a Bug" +
                "\n" + "32.showtaskactivity (Task ID)                                            - Shows task activity" +
                "\n" + "33.assigntasktoboard (Team name) (Board name) (Task ID)                  - Assigns task to a board" +
                "\n" + "34.showboardtasks (Team name) (Board name)                               - Shows all tasks assigned to Board";

        return str;
    }
}
