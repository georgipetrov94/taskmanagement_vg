package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;

import java.util.List;

public class ShowTaskId implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    private static final String WRONG_PARAMETERS_EXCEPTION = "Wrong parameter for Task ID!";
    private static final String NO_TASK_WITH_ID_EXCEPTION = "No task with ID: %s";


    private final TaskManagementRepository taskManagementRepository;

    public ShowTaskId(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);
        int taskID;
        try {
            taskID = Integer.parseInt(parameters.get(0));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(WRONG_PARAMETERS_EXCEPTION);
        }

        return showTask(taskID);
    }

    private String showTask(int taskID) {

        if (taskManagementRepository.getTasks().containsKey(taskID)) {

            return (taskManagementRepository.getTasks().get(taskID).print());
        }

        throw new IllegalArgumentException(String.format(NO_TASK_WITH_ID_EXCEPTION, taskID));
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS,
                            parameters.size()));
        }
    }


}
