package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.constants.CommandConstants;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Priority;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Size;
import com.telerikacademy.oop.taskmanagement.models.contracts.Story;
import com.telerikacademy.oop.taskmanagement.models.tasks.StoryImpl;

import java.util.List;

public class AddStory implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 4;
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    private static final String WRONG_PARAMETERS_EXCEPTION = "Wrong parameters for Priority or/and Size !";


    private final TaskManagementRepository taskManagementRepository;

    public AddStory(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        String name = parameters.get(0);
        String description = parameters.get(1);
        String priority = parameters.get(2);
        String size = parameters.get(3);

        return addStory(name, description, priority, size);
    }

    private String addStory(String name, String description, String priority, String size) {

        Story story;
        try {
            story = new StoryImpl(name, description, Priority.valueOf(priority.toUpperCase()), Size.valueOf(size.toUpperCase()));
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(WRONG_PARAMETERS_EXCEPTION);
        }

        taskManagementRepository.addStory(story);

        return String.format(CommandConstants.NEW_TASK_ADDED, story.getTaskId());
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS,
                            parameters.size()));
        }
    }


}
