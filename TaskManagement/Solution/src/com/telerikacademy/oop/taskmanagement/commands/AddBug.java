package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.constants.CommandConstants;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Priority;
import com.telerikacademy.oop.taskmanagement.models.contracts.*;
import com.telerikacademy.oop.taskmanagement.models.common.enums.Severity;
import com.telerikacademy.oop.taskmanagement.models.tasks.BugImpl;

import java.util.List;

public class AddBug implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 4;
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    private static final String WRONG_PARAMETERS_EXCEPTION = "Wrong parameter for Priority or/and Severity";

    private final TaskManagementRepository taskManagementRepository;

    public AddBug(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }


    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        String name = parameters.get(0);
        String description = parameters.get(1);
        String priority = parameters.get(2);
        String severity = parameters.get(3);

        return addBug(name, description, priority, severity);
    }

    private String addBug(String name, String description, String priority, String severity) {

        Bug bug;
        try {
            bug = new BugImpl(name, description, Priority.valueOf(priority.toUpperCase()), Severity.valueOf(severity.toUpperCase()));
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(e.getMessage() != null ? e.getMessage() : WRONG_PARAMETERS_EXCEPTION);
        }

        taskManagementRepository.addBug(bug);

        return String.format(CommandConstants.NEW_TASK_ADDED, bug.getTaskId());
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS,
                            parameters.size()));
        }
    }
}
