package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.constants.CommandConstants;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.*;

import java.util.List;

public class AssignTaskToPerson implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    private static final String WRONG_PARAMETERS_EXCEPTION = "Wrong parameter for Task ID!";
    private static final String NO_PERSON_WITH_NAME_EXCEPTION = "No person with name %s";
    private static final String NO_TASK_WITH_ID_EXCEPTION = "No task with ID %d or unable to add assignee!";

    private final TaskManagementRepository taskManagementRepository;

    public AssignTaskToPerson(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        String person = parameters.get(0);
        int id;
        try {
            id = Integer.parseInt(parameters.get(1));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(WRONG_PARAMETERS_EXCEPTION);
        }

        return assignTask(person, id);
    }


    private String assignTask(String person, int id) {

        Person person1 = taskManagementRepository.getPeople()
                .stream()
                .filter(u -> u.getName().equalsIgnoreCase(person))
                .findFirst().orElseThrow(() -> new IllegalArgumentException(String.format(NO_PERSON_WITH_NAME_EXCEPTION, person)));
        var assignables= taskManagementRepository.getAssignables();


        if (assignables.containsKey(id)) {

            person1.addTask(assignables.get(id));
            assignables.get(id).setAssignee(person1);

        } else {
            throw new IllegalArgumentException(String.format(NO_TASK_WITH_ID_EXCEPTION, id));
        }


        return String.format(CommandConstants.TASK_ASSIGNED, id, person1.getName());
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS,
                            parameters.size()));
        }
    }

}
