package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Team;

import java.util.List;

public class ShowBoards implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    private static final String NO_TEAM_WITH_NAME_EXCEPTION = "No team with name %s";


    private final TaskManagementRepository taskManagementRepository;

    public ShowBoards(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        String name = parameters.get(0);

        return showAllBoards(name);
    }

    private String showAllBoards(String team) {

        Team team1 = taskManagementRepository.getTeams()
                .stream()
                .filter(u -> u.getName().equalsIgnoreCase(team))
                .findFirst().orElseThrow(() -> new IllegalArgumentException(String.format(NO_TEAM_WITH_NAME_EXCEPTION, team)));

        return team1.showBoards();
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS,
                            parameters.size()));
        }
    }


}
