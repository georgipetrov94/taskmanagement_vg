package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Person;


import java.util.List;

public class ShowPeople implements Command {

    private final TaskManagementRepository taskManagementRepository;

    public ShowPeople(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        return showAllPeople();
    }

    private String showAllPeople() {

        StringBuilder builder = new StringBuilder();
        builder.append("--PEOPLE--").append(System.lineSeparator());
        int counter = 1;
        for (Person person : taskManagementRepository.getPeople()) {
            builder.append(String.format("%d. %s", counter, person.toString()))
                    .append(System.lineSeparator());
            counter++;
        }

        return builder.toString().trim();
    }


}
