package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.*;

import java.util.List;

public class ShowTypeTask implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    private static final String NO_SUCH_TASK_TYPE_EXCEPTION = "No such task type!";

    private final TaskManagementRepository taskManagementRepository;

    public ShowTypeTask(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }


    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        String typeTack = parameters.get(0);

        return showTask(typeTack);
    }

    private String showTask(String typeTack) {

        StringBuilder str = new StringBuilder("------------\n");

        switch (typeTack.toUpperCase()) {
            case "BUGS":
                str.append("Show only Bugs:#######\n");
                for (Bug task : taskManagementRepository.getBugs().values()) {
                    str.append(task.print());
                    str.append("------------\n");
                }
                break;
            case "STORIES":
                str.append("Show only Stories:#######\n");
                for (Story task : taskManagementRepository.getStories().values()) {
                    str.append(task.print());
                    str.append("------------\n");
                }
                break;
            case "FEEDBACKS":
                str.append("Show only Feedbacks:#######\n");
                for (Feedback task : taskManagementRepository.getFeedbacks().values()) {
                    str.append(task.print());
                    str.append("------------\n");
                }
                break;
            default:
                throw new IllegalArgumentException(NO_SUCH_TASK_TYPE_EXCEPTION);

        }

        return str.toString();
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS,
                            parameters.size()));
        }
    }

}
