package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;

import java.util.List;

public class GetTaskActivity implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    private static final String WRONG_PARAMETERS_EXCEPTION = "Wrong parameter for Task ID!";
    private static final String NO_TASK_WITH_THAT_ID_EXCEPTION = "No task with that ID";


    private final TaskManagementRepository taskManagementRepository;

    public GetTaskActivity(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        int taskID;
        try {
            taskID = Integer.parseInt(parameters.get(0));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(WRONG_PARAMETERS_EXCEPTION);
        }

        return getTaskActivity(taskID);
    }

    private String getTaskActivity(int taskID) {

        StringBuilder str = new StringBuilder(String.format("Show Task with ID %d activity:#######\n", taskID));
        if (taskManagementRepository.getTasks().containsKey(taskID)) {
            str.append(taskManagementRepository.getTasks().get(taskID).getHistory());
        } else {
            throw new IllegalArgumentException(NO_TASK_WITH_THAT_ID_EXCEPTION);
        }


        return str.toString();
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS,
                            parameters.size()));
        }
    }


}

