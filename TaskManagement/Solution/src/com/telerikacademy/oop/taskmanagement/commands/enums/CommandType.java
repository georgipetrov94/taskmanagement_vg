package com.telerikacademy.oop.taskmanagement.commands.enums;

public enum CommandType {
    CREATEPERSON,
    CREATETEAM,
    CREATEBOARD,
    SHOWBOARDS,
    CREATEBUG,
    SHOWTASKS,
    SHOWTASKID,
    CREATESTORY,
    CREATEFEEDBACK,
    SHOWPEOPLE,
    SHOWPERSONACTIVITY,
    SHOWTEAMS,
    SHOWTEAMACTIVITY,
    ADDPERSONINTEAM,
    SHOWTEAMMEMBERS,
    SHOWBOARDACTIVITY,
    ASSIGNTASKTOPERSON,
    ASSIGNTASKTOBOARD,
    SHOWBOARDTASKS,
    SHOWTASKTYPE,
    UNASSIGNTASK,
    ADDCOMMENT,
    SHOWPERSONTASKS,
    SORTSTATUS,
    SORTTITLE,
    SORTPRIORITY,
    SORTBUGSEVERITY,
    SORTSTORYSIZE,
    SORTFEEDBACKRATING,
    COMMANDS,
    TASKCHANGE,
    ADDBUGSTEPS,
    SHOWTASKACTIVITY
}
