package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.constants.CommandConstants;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementFactory;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.BoardImpl;
import com.telerikacademy.oop.taskmanagement.models.contracts.Board;
import com.telerikacademy.oop.taskmanagement.models.contracts.Team;

import java.util.List;

public class AddTeam implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";

    private final TaskManagementFactory taskManagementFactory;
    private final TaskManagementRepository taskManagementRepository;

    public AddTeam(TaskManagementFactory taskManagementFactory, TaskManagementRepository taskManagementRepository) {
        this.taskManagementFactory = taskManagementFactory;
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        String name = parameters.get(0);

        return addTeam(name);
    }

    private String addTeam(String name) {

        Team team = taskManagementFactory.createTeam(name);

        Board board = new BoardImpl("BaseBoard");

        team.addBoard(board);

        taskManagementRepository.addTeam(team);

        return String.format(CommandConstants.NEW_TEAM_ADDED, taskManagementRepository.getTeams().get(taskManagementRepository.getTeams().size() - 1));
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS,
                            parameters.size()));
        }
    }

}
