package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Bug;
import com.telerikacademy.oop.taskmanagement.models.contracts.Feedback;
import com.telerikacademy.oop.taskmanagement.models.contracts.Story;
import com.telerikacademy.oop.taskmanagement.models.contracts.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortStatusTasks implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    private static final String NO_SUCH_TASK_TYPE_EXCEPTION = "No such task type!";


    private final TaskManagementRepository taskManagementRepository;

    public SortStatusTasks(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        String typeTask = parameters.get(0);
        return sortStatusTasks(typeTask);

    }

    private String sortStatusTasks(String typeTask) {

        switch (typeTask.toUpperCase()) {
            case "BUGS":
                List<Bug> list1 = new ArrayList<>(taskManagementRepository.getBugs().values());

                return "Sorted by status Bugs#########\n"
                        + list1.stream()
                        .sorted(Comparator.comparing(Bug::getStatus))
                        .map(Task::print)
                        .collect(Collectors.joining());

            case "FEEDBACKS":
                List<Feedback> list2 = new ArrayList<>(taskManagementRepository.getFeedbacks().values());

                return "Sorted by status Feedbacks#########\n"
                        + list2.stream()
                        .sorted(Comparator.comparing(Feedback::getStatus))
                        .map(Task::print)
                        .collect(Collectors.joining());

            case "STORIES":
                List<Story> list3 = new ArrayList<>(taskManagementRepository.getStories().values());

                return "Sorted by status Stories#########\n"
                        + list3.stream()
                        .sorted(Comparator.comparing(Story::getStatus))
                        .map(Task::print)
                        .collect(Collectors.joining());

            default:
                throw new IllegalArgumentException(NO_SUCH_TASK_TYPE_EXCEPTION);
        }
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS,
                            parameters.size()));
        }
    }

}
