package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Board;
import com.telerikacademy.oop.taskmanagement.models.contracts.Team;

import java.util.List;

public class ShowBoardTasks implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    private static final String NO_TEAM_WITH_NAME_EXCEPTION = "No team with name %s";
    private static final String NO_BOARD_WITH_NAME_EXCEPTION = "No board with name %s in team %s";

    private final TaskManagementRepository taskManagementRepository;

    public ShowBoardTasks(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        String team = parameters.get(0);
        String board = parameters.get(1);

        return showBoardTasks(team, board);
    }

    private String showBoardTasks(String team, String board) {

        Team team1 = taskManagementRepository.getTeams()
                .stream().filter(u -> u.getName().equalsIgnoreCase(team))
                .findFirst().orElseThrow(() -> new IllegalArgumentException(String.format(NO_TEAM_WITH_NAME_EXCEPTION, team)));

        Board board1 = team1.getBoards()
                .stream()
                .filter(u -> u.getName().equalsIgnoreCase(board))
                .findFirst().orElseThrow(() -> new IllegalArgumentException(String.format(NO_BOARD_WITH_NAME_EXCEPTION, board, team)));

        return board1.showTasks();
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS,
                            parameters.size()));
        }
    }


}
