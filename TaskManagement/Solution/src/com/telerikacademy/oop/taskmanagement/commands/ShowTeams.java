package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Team;

import java.util.List;

public class ShowTeams implements Command {

    private final TaskManagementRepository taskManagementRepository;

    public ShowTeams(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        return showAllTeams();
    }

    private String showAllTeams() {

        StringBuilder builder = new StringBuilder();
        builder.append("--TEAMS--").append(System.lineSeparator());
        int counter = 1;
        for (Team team : taskManagementRepository.getTeams()) {
            builder.append(String.format("%d. %s", counter, team.toString()))
                    .append(System.lineSeparator());
            counter++;
        }

        return builder.toString().trim();
    }


}
