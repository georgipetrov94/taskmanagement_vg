package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.constants.CommandConstants;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Person;
import com.telerikacademy.oop.taskmanagement.models.contracts.Task;
import com.telerikacademy.oop.taskmanagement.models.contracts.Team;

import java.util.List;

public class AddPersonTeam implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    private static final String NO_TEAM_WITH_NAME_EXCEPTION = "No team with name %s";
    private static final String NO_PERSON_WITH_NAME_EXCEPTION = "No person with name %s";


    private final TaskManagementRepository taskManagementRepository;

    public AddPersonTeam(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        String team = parameters.get(0);
        String person = parameters.get(1);

        return addPersonTeam(team, person);
    }

    private String addPersonTeam(String team, String person) {

        Team team1 = taskManagementRepository.getTeams()
                .stream()
                .filter(u -> u.getName().equalsIgnoreCase(team))
                .findFirst().orElseThrow(() -> new IllegalArgumentException(String.format(NO_TEAM_WITH_NAME_EXCEPTION, team)));

        Person person1 = taskManagementRepository.getPeople()
                .stream()
                .filter(u -> u.getName().equalsIgnoreCase(person))
                .findFirst().orElseThrow(() -> new IllegalArgumentException(String.format(NO_PERSON_WITH_NAME_EXCEPTION, person)));

        team1.addPerson(person1);

        for (Task task : person1.getTasks().values()) {
            team1.getBoards().get(0).addTask(task);
        }

        return String.format(CommandConstants.NEW_MEMBER_ADDED, person1.getName(), team1.getName());
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS,
                            parameters.size()));
        }
    }

}
