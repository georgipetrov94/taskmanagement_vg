package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Story;
import com.telerikacademy.oop.taskmanagement.models.contracts.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortSizeStories implements Command {

    private final TaskManagementRepository taskManagementRepository;

    public SortSizeStories(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        return sortSizeStories();

    }

    private String sortSizeStories() {

        List<Story> list3 = new ArrayList<>(taskManagementRepository.getStories().values());

        return "Sorted by size Stories#########\n"
                + list3.stream()
                .sorted(Comparator.comparing(Story::getSize))
                .map(Task::print)
                .collect(Collectors.joining());

    }



}
