package com.telerikacademy.oop.taskmanagement.commands;

import com.telerikacademy.oop.taskmanagement.commands.constants.CommandConstants;
import com.telerikacademy.oop.taskmanagement.commands.contracts.Command;
import com.telerikacademy.oop.taskmanagement.core.contracts.TaskManagementRepository;
import com.telerikacademy.oop.taskmanagement.models.contracts.Board;
import com.telerikacademy.oop.taskmanagement.models.contracts.Task;
import com.telerikacademy.oop.taskmanagement.models.contracts.Team;

import java.util.List;

public class AssignTaskToBoard implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    private static final String NO_TEAM_WITH_NAME_EXCEPTION = "No team with name %s";
    private static final String NO_BOARD_WITH_NAME_EXCEPTION = "No board with name %s in team %s";
    private static final String WRONG_PARAMETERS_EXCEPTION = "Wrong parameter for Task ID!";
    private static final String NO_TASK_WITH_ID_EXCEPTION = "No task with ID %d";

    private final TaskManagementRepository taskManagementRepository;

    public AssignTaskToBoard(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        String team = parameters.get(0);
        String board = parameters.get(1);
        int id;
        try {
            id = Integer.parseInt(parameters.get(2));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(WRONG_PARAMETERS_EXCEPTION);
        }

        return assignTask(team, board, id);
    }


    private String assignTask(String team, String board, int id) {

        Team team1 = taskManagementRepository.getTeams()
                .stream()
                .filter(u -> u.getName().equalsIgnoreCase(team))
                .findFirst().orElseThrow(() -> new IllegalArgumentException(String.format(NO_TEAM_WITH_NAME_EXCEPTION, team)));

        Board board1 = team1.getBoards()
                .stream()
                .filter(u -> u.getName().equalsIgnoreCase(board))
                .findFirst().orElseThrow(() -> new IllegalArgumentException(String.format(NO_BOARD_WITH_NAME_EXCEPTION, board, team)));

        if (taskManagementRepository.getTasks().containsKey(id)) {

            Task task = taskManagementRepository.getTasks().get(id);
            board1.addTask(task);

        } else {
            throw new IllegalArgumentException(String.format(NO_TASK_WITH_ID_EXCEPTION, id));
        }


        return String.format(CommandConstants.TASK_ASSIGNED_BOARD, id, board1.getName(), team1.getName());
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS,
                            parameters.size()));
        }
    }

}
