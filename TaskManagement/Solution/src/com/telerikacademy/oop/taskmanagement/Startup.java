package com.telerikacademy.oop.taskmanagement;

import com.telerikacademy.oop.taskmanagement.core.TaskManagementEngineImpl;

public class Startup {
    
    public static void main(String[] args) {
        System.out.println("Type 'commands' for the list of commands !");
        TaskManagementEngineImpl engine = new TaskManagementEngineImpl();
        engine.start();
    }
    
}
